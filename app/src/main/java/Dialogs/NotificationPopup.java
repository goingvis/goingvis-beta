package Dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.example.felix.goingvis_prototype.Notifications;
import com.example.felix.goingvis_prototype.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import model.UserData;
import RestActions.ClearNotifications;

/**
 * Created by felix on 1/25/17.
 */

public class NotificationPopup extends android.support.v4.app.DialogFragment {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    TabLayout tabLayout;
    int tabCount = 3;
    FloatingActionButton new_post_button;
    int currentPosition = -1;
    Context ac;
    UserData user;
    View rootView;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.notification_popup, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        user = (UserData) getActivity().getApplicationContext();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) rootView.findViewById(R.id.pagerNotification);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        Log.d("Pager is set WOoot!","Pager");

        // Set up Tabs
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabsNotification);
        tabLayout.setupWithViewPager(mViewPager);
        //mSectionsPagerAdapter.getItem(0);
        return rootView;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Log.d("SetUpItem",String.valueOf(position));
            if(position == 0)
            {
                return Notifications.newInstance("interaction","interaction");
            }
            else if(position == 1 )
            {
                return Notifications.newInstance("message","message");
            }
            else{
                return Notifications.newInstance("important","important");
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return tabCount;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Interaction";
                case 1:
                    return "Inbox";
                case 2:
                    return "Important";
                default:
                    return  "None";
            }

        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        new ClearNotifications().execute(user.id());
    }
}
