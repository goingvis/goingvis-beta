package Dialogs;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.felix.goingvis_prototype.R;

import org.json.JSONException;

import model.UserData;
import RestActions.DeleteMicropost;

/**
 * Created by felix on 11/12/16.
 */

public class MicropostOptions extends DialogFragment {
    UserData user;
    Button delete;
    Button share;
    String toShare;

    public MicropostOptions(String s){
        toShare = s;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.micropost_options_popup, container, false);
        // Set up for larger Dialog


        // Getting rid of the divider
        int divierId = getDialog().getContext().getResources()
                .getIdentifier("android:id/titleDivider", null, null);
        View divider = getDialog().findViewById(divierId);
        if(divider != null)
        {
            divider.setVisibility(View.GONE);
        }

        // Setting Spinner Color
        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.deleteLoad);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#1fb24a"),android.graphics.PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.GONE);

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        Log.d("Width", Integer.toString(width));
        Log.d("Height",Integer.toString(height));
        getDialog().getWindow().setLayout(width - 100, ViewGroup.LayoutParams.WRAP_CONTENT);

        getDialog().setTitle("Options");
        user = (UserData) getActivity().getApplicationContext();
        delete = (Button) rootView.findViewById(R.id.deleteMicropost);
        share = (Button) rootView.findViewById(R.id.shareContent);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, user.textToSend());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        if(user.getCanDelete())
        {
            delete.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    delete.setEnabled(false);
                    share.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                    DeleteMicropost asyncTask = (DeleteMicropost) new DeleteMicropost(new DeleteMicropost.AsyncResponse(){

                        @Override
                        public void processFinish(String output) throws JSONException {
                            delete.setEnabled(true);
                            share.setEnabled(true);
                            progressBar.setVisibility(View.GONE);
                            getDialog().dismiss();
                        }
                    }).execute(user.temp,user.positionTemp);
                }
            });
        }
        else
        {
            delete.setVisibility(View.GONE);
        }

        return rootView;
    }
}

