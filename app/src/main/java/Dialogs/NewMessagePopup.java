package Dialogs;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;

import com.example.felix.goingvis_prototype.MainActivity;
import com.example.felix.goingvis_prototype.MyReceiver;
import com.example.felix.goingvis_prototype.R;

import org.json.JSONException;

import model.UserData;
import RestActions.CreateConversation;

/**
 * Created by felix on 12/13/16.
 */

public class NewMessagePopup extends DialogFragment  {
    UserData user;
    Button sendMessage;
    EditText subject,message;
    MultiAutoCompleteTextView recepients;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        View rootView = inflater.inflate(R.layout.new_message_popup, container, false);
        // Set up for larger Dialog


        // Getting rid of the divider
        int divierId = getDialog().getContext().getResources()
                .getIdentifier("android:id/titleDivider", null, null);
        View divider = getDialog().findViewById(divierId);
        if (divider != null) {
            divider.setVisibility(View.GONE);
        }

        // Setting Spinner Color
        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.sendMessageLoad);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#1fb24a"), android.graphics.PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.GONE);

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        Log.d("Width", Integer.toString(width));
        Log.d("Height", Integer.toString(height));
        getDialog().getWindow().setLayout(width - 100, ViewGroup.LayoutParams.WRAP_CONTENT);

        user = (UserData) getActivity().getApplicationContext();
        sendMessage = (Button) rootView.findViewById(R.id.sendMessageButton);
        recepients = (MultiAutoCompleteTextView) rootView.findViewById(R.id.newMessageRecepients);
        subject = (EditText) rootView.findViewById(R.id.newMessageSubject);
        message = (EditText) rootView.findViewById(R.id.newMessageBody);


        Log.d("pop",user.friends.toString());
        for(int i = 0; i < user.friends.size(); i++){Log.d("inPOP",user.friendsArray[i]);}
        recepients.setTokenizer( new MultiAutoCompleteTextView.CommaTokenizer() );
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, user.friendsArray);

        //Find TextView control
        //Set the number of characters the user must type before the drop down list is shown
        recepients.setThreshold(1);
        //Set the adapter
        recepients.setAdapter(adapter);

        // Send message on click listener
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                subject.setClickable(false);
                message.setClickable(false);
                sendMessage.setClickable(false);
                recepients.setClickable(false);
                new CreateConversation(new CreateConversation.AsyncResponse() {
                    @Override
                    public void processFinish(String output) throws JSONException {
                        progressBar.setVisibility(View.GONE);
                        Intent openFrag = new Intent(getActivity(), MainActivity.class);
                        Log.d("CreateConvo",output);
                        if(output.contains("\n") || output.contains("\r") )
                        {
                            output = output.replace("\n", "").replace("\r", "");
                        }
                        String [] arr = new String [] {"conversation",output};
                        openFrag.putExtra("fragment",arr);
                        user.setCurrentConversation(output);
                        new MyReceiver().onReceive(getActivity(), openFrag);
                    }
                }).execute(user.username(),subject.getText(),message.getText(),recepients.getText());
            }
        });

        recepients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Recepients","Has been clicked");
            }
        });


        return rootView;
    }
}
