package Dialogs;


import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.felix.goingvis_prototype.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;

import Classes.Utility;
import RestActions.GetUserFeed;
import RestActions.NewMicropost;
import model.UserData;

import static android.app.Activity.RESULT_OK;

/**
 * Created by felix on 10/17/16.
 */

public class NewPostFragment extends DialogFragment {
    UserData user;
    EditText messageEditText;
    //Button facebook;
   // Button twitter;
    //MultiAutoCompleteTextView input;
    boolean sendToFacebook = false;
    boolean sendToTwitter = false;
    private static int RESULT_LOAD_IMAGE = 1;
    ImageView uploadImage, btn_close_dialogue;
    View rootView;
    String baseImg = "";
    Button post;
    ProgressBar progressBar;
    File pictureFile = null;
    String picturePath;
    String twit;
    String face;
    //MultiAutoCompleteTextView recepients;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_theme);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_new_post, container, false);
        //input = (MultiAutoCompleteTextView) rootView.findViewById(R.id.post_content);
        messageEditText = (EditText)rootView.findViewById(R.id.messageEditText);
        btn_close_dialogue = (ImageView) rootView.findViewById(R.id.removeImageView);
        user = (UserData) getActivity().getApplicationContext();

        if(user.friendsArray!=null){
            //input.setTokenizer( new SpaceTokenizer());
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, user.friendsArray);

            //Find TextView control
            //Set the number of characters the user must type before the drop down list is shown
            //input.setThreshold(1);
            //Set the adapter
            //input.setAdapter(adapter);
        }

        // Set width of the dialog box
        int width = getResources().getDisplayMetrics().widthPixels - 100;
        int height = getResources().getDisplayMetrics().heightPixels - 200;
        getDialog().getWindow().setLayout(width, height);

        // Getting rid of the divider
        int divierId = getDialog().getContext().getResources()
                .getIdentifier("android:id/titleDivider", null, null);
        View divider = getDialog().findViewById(divierId);
        if(divider != null)
        {
            divider.setVisibility(View.GONE);
        }

        View layout = (View) rootView.findViewById(R.id.socialSendLayout);
        //input = (TextView) rootView.findViewById(R.id.post_content);
        String activeButton = "";
        int activeCount = 0;
        //facebook = (Button) rootView.findViewById(R.id.facebook);
        //twitter = (Button) rootView.findViewById(R.id.twitter);
        /*if(user.facebookActive() || user.twitterActive())
        {
            Log.d("Facebook or Twitter","YAY");
            if(user.facebookActive())
            {
                activeCount++;
                activeButton = "face";
                facebook.setVisibility(View.VISIBLE);
                facebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        if(sendToFacebook)
                        {
                            sendToFacebook = false;
                            facebook.setTextColor(Color.parseColor("#ffffff"));
                        }
                        else
                        {
                            sendToFacebook = true;
                            facebook.setTextColor(Color.parseColor("#000000"));
                        }
                    }
                });
            }
            if(user.twitterActive())
            {
                activeCount++;
                activeButton = "twit";
                twitter.setVisibility(View.VISIBLE);
                twitter.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(sendToTwitter){
                            sendToTwitter = false;
                            twitter.setTextColor(Color.parseColor("#ffffff"));
                        }
                        else{
                            sendToTwitter = true;
                            twitter.setTextColor(Color.parseColor("#000000"));
                        }
                    }
                });
            }
            if(activeCount == 1)
            {
                // Only one is active
                switch (activeButton)
                {
                    case "face":
                        facebook.setGravity(Gravity.CENTER);
                        twitter.setVisibility(View.GONE);
                        break;
                    case "twit":
                        Log.d("center twitter","about to center");
                        twitter.setGravity(Gravity.CENTER);
                        facebook.setVisibility(View.GONE);
                        break;
                }
            }
            layout.setVisibility(View.VISIBLE);
        }
        else
        {
            layout.setVisibility(View.GONE);
            TextInputLayout inputLayout = (TextInputLayout) rootView.findViewById(R.id.textPostInput);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0,10,0,0);
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            inputLayout.setLayoutParams(params);
        }*/

        btn_close_dialogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                getDialog().dismiss();

            }
        });

        // Set Image upload
        uploadImage = (ImageView) rootView.findViewById(R.id.addImageToPost);
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload_image(v);
            }
        });

        // Setting Spinner Color
        progressBar = (ProgressBar) rootView.findViewById(R.id.post_load);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#1fb24a"),android.graphics.PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.GONE);
        post = (Button) rootView.findViewById(R.id.post);

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                messageEditText.setEnabled(false);
                twit = "";
                face = "";
                if(sendToTwitter){
                    twit = "&micropost[send_twitter]=1";
                }
                if(sendToFacebook){
                    face = "&micropost[send_facebook]=1";
                }
                if(pictureFile != null)
                {
                    if(shouldAskPermission())
                    {
                        Log.d("WillAskPermission","About to ask permission");
                        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
                        int permsRequestCode = 200;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            //ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                            //getParentFragment().requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},101);
                            Log.d("PermissionAsked!!!!!!!","Wait for repsonse");
                            //sendPictureRestAction();
                        }
                    }
                    else
                    {
                        sendPictureRestAction();
                    }

                }
                else
                {
                    NewMicropost asyncTask = (NewMicropost) new NewMicropost(new GetUserFeed.AsyncResponse(){
                        @Override
                        public void processFinish(String output) throws JSONException {
                            clearPopup();
                        }
                    }).execute(messageEditText.getText().toString(),user.username(),twit + face);
                }

            }
        });

        getDialog().setTitle("New Post");

        /*
        Future<JsonObject> js  = Ion.with(getActivity().getApplicationContext())
                .load("https://www.goingvis.com/api/v1/get_friends")
                .setLogging("MyLogs", Log.DEBUG)
                .addQuery("username","Felix")
                .addQuery("api_key",Utility.apiKey)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if(result != null) {
                            Log.d("DONE!", result.getAsString());
                        }
                    }
                });
         */

        return rootView;
    }

    void sendPictureRestAction()
    {
        Future<JsonObject> js  = Ion.with(getActivity().getApplicationContext())
                .load("https://www.goingvis.com/api/v1/new_micropost")
                .setLogging("MyLogs", Log.DEBUG)
                .setMultipartFile("micropost[picture]", new File(picturePath))
                .setMultipartParameter("micropost[content]",messageEditText.getText().toString())
                .setMultipartParameter("micropost[authors_username]",user.username)
                .setMultipartParameter("api_key",Utility.apiKey)
                .setMultipartParameter("",twit)
                .setMultipartParameter("",face)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if(result != null) {
                            Log.d("DONE!", result.getAsString());
                        }
                        clearPopup();
                    }
                });
    }

    void clearPopup(){
        messageEditText.setText("");
        post.setEnabled(true);
        messageEditText.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        getDialog().dismiss();
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        Dialog d = getDialog();
        if (d!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }

        TextView textView = (TextView) this.getDialog().findViewById(android.R.id.title);
        if(textView != null)
        {
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(getResources().getColor(R.color.black));
        }
    }

    void upload_image(View v)
    {
        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            try {

                Bitmap tempBit;
                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(selectedImage,filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                //tempBit = BitmapFactory.decodeFile(picturePath);

                pictureFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath(), picturePath);

                //baseImg = Utility.encodeToBase64(tempBit,Bitmap.CompressFormat.PNG,70);
                Log.d("BASEIMG",baseImg);
                uploadImage.setImageBitmap(decodeUri(selectedImage));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            /*
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            uploadImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            */
            //ImageView imageView = (ImageView) findViewById(R.id.imgView);
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                getActivity().getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                getActivity().getApplicationContext().getContentResolver().openInputStream(selectedImage), null, o2);
    }

    // Asking permission in newer versions of android
    private boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        Log.d("onRequestPermission","Wait for resulsts");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        sendPictureRestAction();
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }


}