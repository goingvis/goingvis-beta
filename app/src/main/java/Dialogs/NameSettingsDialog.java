package Dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.felix.goingvis_prototype.R;

import model.UserData;

/**
 * Created by felix on 9/28/16.
 */

public class NameSettingsDialog extends DialogFragment {
    UserData user;
    TextView username;
    TextView name;
    TextView pass;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.name_settings_popup, container, false);
            user = (UserData) getActivity().getApplicationContext();
            username = (TextView) rootView.findViewById(R.id.username_input);
            name = (TextView) rootView.findViewById(R.id.full_name_input);
            pass = (TextView) rootView.findViewById(R.id.password_setting_input);
            username.setText(user.username());
            name.setText(user.name());
            getDialog().setTitle("Update your information");
            return rootView;
        }
}
