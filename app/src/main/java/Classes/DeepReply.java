package Classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by felix on 11/19/16.
 */

public class DeepReply {
    Hashtable<String,String> hashtable;
    public String love_count,hate_count;


    public String getContent(){
        return hashtable.get("message").toString();
    }
    public String getUrl(){return "URL";}
    public String getTimeStamp(){
        return hashtable.get("created_at").toString();
    }
    public String getId(){return hashtable.get("id").toString();}
    public String getHateCount(){return hate_count;}
    public String getLoveCount(){return  love_count;}
    public String getCommentCount(){return hashtable.get("comments_count").toString();}
    public String getImg(){
        return "";
    }
    public DeepReply(){}
    public DeepReply(Hashtable<String,String> tb)
    {
        this.hashtable = tb;
    }
    public String getReplyId(){return hashtable.get("reply_id").toString();}

    public String getName(){
        return hashtable.get("authors_full_name").toString() + " @"  + hashtable.get("authors_username").toString();
    }

    public String getProfilePicture(){
        String pic = hashtable.get("authors_picture").toString();
        if(pic.equals("default-user.png")){
            return "https://s3.amazonaws.com/goingvisphotos/website_photos/default-user.png";
        }else{
            return pic;
        }
    }

    public void set_deep_reply (String data) throws JSONException {
        hashtable = new Hashtable<>();
        JSONObject json = new JSONObject(data);
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            hashtable.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        love_count = hashtable.get("love_count");
        hate_count = hashtable.get("hate_count");
    }

}
