package Classes;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * Created by felix on 11/30/16.
 */

public class Product {
    public Hashtable<String,String> productHash = new Hashtable<>();
    public String dataStr;

    public void set_product (String data) throws JSONException {
        dataStr = data;
        JSONObject json = new JSONObject(data);
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            productHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
    }

    public String [] galleryImages() throws JSONException {
        ArrayList<String> im = new ArrayList<>();
        JSONObject jsonPic = new JSONObject(productHash.get("pictures"));
        Hashtable<String,String> table = Utility.jsonToHash(jsonPic);
        for(int i = 0; i < 5 ;i++){
            if(table.get(Integer.toString(i)) != null && table.get(Integer.toString(i)).length() > 4 )
            {
                Log.d("PRODUCT",table.get(Integer.toString(i)));
                im.add(table.get(Integer.toString(i)));
            }
        }
        String[] array = im.toArray(new String[0]);
        return array;
    }

}
