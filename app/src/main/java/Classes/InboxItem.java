package Classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by felix on 12/6/16.
 */

public class InboxItem {
    String lastMessage;
    String subject;
    String date;
    public Hashtable<String,String> inboxHash = new Hashtable<>();

    public void set_inbox_item (String data) throws JSONException {
        JSONObject json = new JSONObject(data);
        for (int i = 0; i < json.names().length(); i++) {
            inboxHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
    }
}
