package Classes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.felix.goingvis_prototype.MainActivity;
import com.example.felix.goingvis_prototype.MyReceiver;
import com.example.felix.goingvis_prototype.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;

import java.util.Map;

import model.LinkPreview;
import model.User;
import model.UserData;
import UtilityFunctions.NavBar;
import UtilityFunctions.SaveSharedPreference;

/**
 * Created by felix on 8/9/16.
 */
public class Utility {
    static String gvs_key = "cXjlQE5k70dRkxU74A8KBDPIkmUtkVpC";
    static public String apiUrl = "&api_key=cXjlQE5k70dRkxU74A8KBDPIkmUtkVpC";
    static public String newsFeedDatabase = "https://gvs-newsfeed.firebaseio.com/";
    static public String apiKey = "cXjlQE5k70dRkxU74A8KBDPIkmUtkVpC";
    static public String username;
    public RequestQueue mRequestQueue;
    public ImageLoader mImageLoader;
    static public double dpiNewsFeedMarginTop = 19.9;

    static boolean setup(Activity activity, Hashtable<String,String> userhash){
        if (userhash == null) {
            return false;
        }else{
            NavBar navbar = new NavBar(activity, userhash);
            return true;
        }
    }

    // Checks if a user already has a saved application
    // Pre: None
    // Post: True if a state is saved, false if not user exists
    static public boolean already_logged_in(Activity activity){
        UserData user = (UserData) activity.getApplicationContext();
        boolean saved = user.getSaved();
        if(SaveSharedPreference.getUserName(activity).length() == 0)
        {
            return false;
        }
        else
        {
            username = SaveSharedPreference.getUserName(activity);
            return true;
        }
    }

    static public void logout(Activity activity){
        SaveSharedPreference.setUserName(activity,"");
    }

    static public ColorDrawable modalDismiss(FrameLayout frame){
        frame.getForeground().setAlpha(0);
        return new ColorDrawable();
    }

    //Clean URL
    //
    static public String cleanUrl (String url){
        if(url.contains(" ")){
            url = url.replace(" ","%20");
        }
        if(url.contains("+")){
            url = url.replace("+","%2B");
        }
        return url;
    }

    static public Hashtable<String, String> jsonToHash(JSONObject json) throws JSONException {
        Hashtable<String, String> hash = new Hashtable<String, String>();
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            hash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        return hash;
    }

    static public Hashtable<String,String> singleRestOutputToHash(String output) throws JSONException {
        Hashtable<String,String> temp = new Hashtable<>();
        JSONObject json = new JSONObject(output);
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            temp.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        return temp;
    }

    static public Hashtable<String,String> jsonFirebaseToHash(JSONObject json) throws JSONException {
        Hashtable<String, String> hash = new Hashtable<String, String>();
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            hash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        Map.Entry<String,String> entry=hash.entrySet().iterator().next();
        JSONObject d = new JSONObject(entry.getValue());
        Hashtable<String,String> specificHash = Utility.jsonToHash(d);
        return specificHash;
    }

    public ImageLoader set_image_loader(Activity context){
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
        return mImageLoader;
    }

    static public String contentExtrasMicropost(Micropost m)
    {
        if(m.micropostHash.get("youtube_url") == null && m.micropostHash.get("links") == null && m.micropostHash.get("soundcloud_url") == null)
        {
            return null;
        }
        if( m.micropostHash.get("youtube_url") != null && m.micropostHash.get("youtube_url").length() > 4)
        {
            return "okay";
        }
        else if(m.micropostHash.get("links") != null && m.micropostHash.get("links").length() > 4)
        {
            return "okay";
        }
        else if( m.micropostHash.get("soundcloud_url") != null && m.micropostHash.get("soundcloud_url").length() > 4)
        {
            return "okay";
        }
        else
        {
            return null;
        }
    }

    static public LinkPreview contentExtrasMicropostLink(Micropost m) throws JSONException {
        if(m.micropostHash.get("youtube_url") != null && m.micropostHash.get("youtube_url").length() > 4)
        {
            LinkPreview lp =  new LinkPreview(m.micropostHash.get("youtube_title"), m.micropostHash.get("youtube_url"), "youtube",m.micropostHash.get("youtube_pic"),"");
            lp.youtube = true;
            return lp;
        }
        else if(m.micropostHash.get("link_preview_pic") != null && m.micropostHash.get("link_preview_pic").length() > 4)
        {
            return new LinkPreview(m.micropostHash.get("link_title"),m.micropostHash.get("links"),"linkPreview",m.micropostHash.get("link_preview_pic"),m.micropostHash.get("link_description"));
        }
        else if(m.micropostHash.get("soundcloud_url") != null && m.micropostHash.get("soundcloud_url").length() > 4)
        {
            return new LinkPreview("Soundcloud Link",m.micropostHash.get("soundcloud_url"),"soundcloud","","Click to Listen");
        }
        else
        {
            return null;
        }

    }

    private String youtubeLinkPic(String url)
    {
        return "title";
    }

    // Handles logic for creating links to @users and hashtags
    static public SpannableString newsFeedContentExtras(final String content, final Activity activity)
    {
        SpannableString ss = new SpannableString(content);
        // Contains an at mention
        if(content.contains("@"))
        {
            int startIndex = 0,endIndex = 0;
            boolean atMentionSearch = false;
            boolean createIt = false;
            for(int i = 0; i < content.length(); i++)
            {
                if(content.charAt(i) == '@' && (i+1 < content.length() ) )
                {
                    startIndex = i;
                    atMentionSearch = true;
                }
                if(atMentionSearch)
                {
                    if( content.charAt(i) == ' ' || i == content.length() - 1)
                    {
                        if(i == content.length() - 1) { endIndex = i+1; }
                        else{ endIndex = i;}
                        atMentionSearch = false;
                        createIt = true;
                    }
                }

                if(createIt)
                {
                    // Now will create the at mention.
                    ClickableSpan clickableSpan = new ClickableSpan()
                    {
                        @Override
                        public void onClick(View textView)
                        {
                            //startActivity(new Intent(NewsFeed.this, NextActivity.class));
                            TextView tv = (TextView) textView;
                            Spanned s = (Spanned) tv.getText();
                            int start = s.getSpanStart(this);
                            int end = s.getSpanEnd(this);
                            String username = s.subSequence(start + 1, end).toString();
                            Log.e("User was clicked",s.subSequence(start, end).toString());
                            Intent openFrag = new Intent(activity,MainActivity.class);
                            String [] arr = new String [] {"newsfeed",username};
                            openFrag.putExtra("fragment",arr);
                            new MyReceiver().onReceive(activity, openFrag);
                        }

                        @Override
                        public void updateDrawState(TextPaint ds)
                        {
                            super.updateDrawState(ds);
                            ds.setUnderlineText(false);
                            ds.setColor(activity.getResources().getColor(R.color.gvs_green));
                        }
                    };
                    ss.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    createIt = false;
                }
            }

        }
        return ss;
    }

    // Returns true if is friend and returns false if
    // they are not friends
    public static String followOrUnfollow(UserData userOne,User userTwo)
    {
        boolean added = false;
        for(int i = 0; i < userOne.follow_ids.length; i++)
        {
            Log.d("Inside of IDS",userOne.follow_ids[i].toString());
            if(userOne.follow_ids[i].equalsIgnoreCase(userTwo.id()))
            {
                added = true;
            }
        }

        if(added) { return "Unfollow"; }
        else { return "Follow"; }
    }

    public static ArrayList<String> setStars(User user)
    {
        ArrayList<String> ratingArr = new ArrayList<>();

        double averageRating = Double.parseDouble(user.userHash.get("average_reviews"));

        if(averageRating <= 0.0)
        {
            for(int i = 0; i < 5; i++)
            {
                ratingArr.add("greystar");
            }
            return ratingArr;
        }
        if(averageRating < 1)
        {
            if(averageRating >= .25 && averageRating < .50)
            {
                ratingArr.add("s1_44");
            }
            else if(averageRating >= .50 && averageRating < .75)
            {
                ratingArr.add("s1_22");
            }
            else
            {
                ratingArr.add("s3_4");
            }
            addGreyStar(ratingArr,4);
        }
        else if(averageRating >= 1 && averageRating < 2)
        {
            addGreenStar(ratingArr,1);
            if(averageRating < 1.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 1.25 && averageRating < 1.50)
            {
                ratingArr.add("s1_44");
            }
            else if(averageRating >= 1.50 && averageRating < 1.75)
            {
                ratingArr.add("s1_2");
            }
            else
            {
                ratingArr.add("s3_44");
            }
            addGreyStar(ratingArr,3);
        }
        else if(averageRating >= 2 && averageRating < 3)
        {
            addGreenStar(ratingArr,2);
            if(averageRating < 2.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 2.25 && averageRating < 2.50)
            {
                ratingArr.add("s1_4");
            }
            else if(averageRating >= 2.50 && averageRating < 2.75)
            {
                ratingArr.add("s1_22");
            }
            else
            {
                ratingArr.add("s3_44");
            }
            addGreyStar(ratingArr,2);
        }
        else if(averageRating >= 3 && averageRating < 4)
        {
            addGreenStar(ratingArr,3);
            if(averageRating < 3.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 3.25 && averageRating < 3.50)
            {
                ratingArr.add("s1_4");
            }
            else if(averageRating >= 3.50 && averageRating < 3.75)
            {
                ratingArr.add("s1_2");
            }
            else
            {
                ratingArr.add("s3_4");
            }
            addGreyStar(ratingArr,1);
        }
        else if( averageRating >= 4 && averageRating < 5)
        {
            addGreenStar(ratingArr,4);
            if(averageRating < 4.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 4.25 && averageRating < 4.50)
            {
                ratingArr.add("s1_4");
            }
            else if(averageRating >= 4.50 && averageRating < 4.75)
            {
                ratingArr.add("s1_2");
            }
            else
            {
                ratingArr.add("s3_44");
            }
        }
        else
        {
            addGreenStar(ratingArr,5);
        }
        return ratingArr;
    }

    public static ArrayList<String> setStarsProduct(Product product)
    {
        ArrayList<String> ratingArr = new ArrayList<>();

        double averageRating = Double.parseDouble(product.productHash.get("average_rating"));
        Log.d("Avara",product.productHash.get("rating"));
        if(averageRating <= 0.0)
        {
            for(int i = 0; i < 5; i++)
            {
                ratingArr.add("greystar");
            }
            return ratingArr;
        }
        if(averageRating < 1)
        {
            if(averageRating >= .25 && averageRating < .50)
            {
                ratingArr.add("s1_44");
            }
            else if(averageRating >= .50 && averageRating < .75)
            {
                ratingArr.add("s1_22");
            }
            else
            {
                ratingArr.add("s3_4");
            }
            addGreyStar(ratingArr,4);
        }
        else if(averageRating >= 1 && averageRating < 2)
        {
            addGreenStar(ratingArr,1);
            if(averageRating < 1.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 1.25 && averageRating < 1.50)
            {
                ratingArr.add("s1_44");
            }
            else if(averageRating >= 1.50 && averageRating < 1.75)
            {
                ratingArr.add("s1_2");
            }
            else
            {
                ratingArr.add("s3_44");
            }
            addGreyStar(ratingArr,3);
        }
        else if(averageRating >= 2 && averageRating < 3)
        {
            addGreenStar(ratingArr,2);
            if(averageRating < 2.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 2.25 && averageRating < 2.50)
            {
                ratingArr.add("s1_4");
            }
            else if(averageRating >= 2.50 && averageRating < 2.75)
            {
                ratingArr.add("s1_22");
            }
            else
            {
                ratingArr.add("s3_44");
            }
            addGreyStar(ratingArr,2);
        }
        else if(averageRating >= 3 && averageRating < 4)
        {
            addGreenStar(ratingArr,3);
            if(averageRating < 3.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 3.25 && averageRating < 3.50)
            {
                ratingArr.add("s1_4");
            }
            else if(averageRating >= 3.50 && averageRating < 3.75)
            {
                ratingArr.add("s1_2");
            }
            else
            {
                ratingArr.add("s3_4");
            }
            addGreyStar(ratingArr,1);
        }
        else if( averageRating >= 4 && averageRating < 5)
        {
            addGreenStar(ratingArr,4);
            if(averageRating < 4.25)
            {
                addGreyStar(ratingArr,1);
            }
            else if(averageRating >= 4.25 && averageRating < 4.50)
            {
                ratingArr.add("s1_4");
            }
            else if(averageRating >= 4.50 && averageRating < 4.75)
            {
                ratingArr.add("s1_2");
            }
            else
            {
                ratingArr.add("s3_44");
            }
        }
        else
        {
            addGreenStar(ratingArr,5);
        }
        return ratingArr;
    }


    // Function is responsible for setting the background resource for the stars
    public static void setBackgroundStar(ArrayList<String> stars, View convertView)
    {

        for(int i = 0; i < stars.size(); i++)
        {
            Log.d("Star",stars.get(i));
            ImageView imageView = (ImageView) convertView.findViewWithTag("star" + Integer.toString(i));
            String type = stars.get(i);
            if(type.equalsIgnoreCase("greenstar"))
            {
                imageView.setImageResource(R.drawable.greenstar);
            }
            else if(type.equalsIgnoreCase("greystar"))
            {
                imageView.setImageResource(R.drawable.greystar);
            }
            else if(type.equalsIgnoreCase("s1_2"))
            {
                imageView.setImageResource(R.drawable.s1_2);
            }
            else if(type.equalsIgnoreCase("s3_4"))
            {
                imageView.setImageResource(R.drawable.s3_4);
            }
            else
            {
                imageView.setImageResource(R.drawable.s1_4);
            }
        }
    }

    public static void addGreyStar(ArrayList<String> arr,int num)
    {
        for(int i = 0; i < num; i++)
        {
            arr.add("greystar");
        }
    }

    public static void addGreenStar(ArrayList<String> arr,int num)
    {
        for(int i = 0; i < num; i++)
        {
            arr.add("greenstar");
        }
    }

    public static String setProductPicture(Product p) throws JSONException {
        JSONObject jsonPic = new JSONObject(p.productHash.get("picture"));
        JSONObject jsonCrop = new JSONObject(Utility.jsonToHash(jsonPic).get("cropped"));
        Hashtable<String,String> last = Utility.jsonToHash(jsonCrop);
        return last.get("url");
    }

    public static double dpToPx(double dp,Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static double pxToDp (double px,Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String findRootUrl(String url){
        Log.d("findRootFor",url);
        String [] splitted = url.split("/");
        String fin = splitted[2];
        if(splitted[2].contains("www.")){
            fin = splitted[2].substring(4,splitted[2].length());
        }
        Log.d("Splitted",splitted.toString());
        return fin;
    }

    public static int screenWidth(Context context){
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int screenHeight(Context context){
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static String notificationType(Hashtable<String,String> table)
    {
        String returnType = "";
        try {

            if(table.get("link").contains("conversation"))
            {
                returnType = "conversation";
            }
            if((table.get("link").contains("micropost")))
            {
                returnType = "micropost";
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return returnType;
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static String createLink(String controller, String id)
    {
        return "https://www.goingvis.com/" + controller + id;
    }


}
