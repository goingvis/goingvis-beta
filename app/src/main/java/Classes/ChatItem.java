package Classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by felix on 12/11/16.
 */

public class ChatItem {
    public String message;
    public String name;
    public String date;
    public String picture;
    public Hashtable<String,String> chatHash = new Hashtable<>();

    public void set_chat_item (String data) throws JSONException {
        JSONObject json = new JSONObject(data);
        for (int i = 0; i < json.names().length(); i++) {
            chatHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        picture = chatHash.get("authors_picture");
        name = chatHash.get("authors_full_name") + " @" + chatHash.get("authors_username");
        message = chatHash.get("message");
        date = chatHash.get("created_at");
    }

    public void set_chat_item_hash(Hashtable<String,String> table)
    {
        picture = table.get("authors_picture");
        name = table.get("authors_full_name") + " @" + table.get("authors_username");
        message = table.get("message");
        date = table.get("created_at");
    }
}
