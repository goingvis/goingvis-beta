package Classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by felix on 12/4/16.
 */

public class Variation {
    public int quantity;
    public String option;
    public String price;
    String id;
    public boolean active = false;
    public Hashtable<String,String> variationHash = new Hashtable<>();

    public void set_variation (String data) throws JSONException {
        JSONObject json = new JSONObject(data);
        for(int i = 0; i<json.names().length(); i++){
            variationHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        quantity = Integer.parseInt(variationHash.get("quantity"));
        option  = variationHash.get("option");
        price = variationHash.get("price_decimal");
        id = variationHash.get("id");

    }
}
