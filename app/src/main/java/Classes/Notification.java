package Classes;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by felix on 1/25/17.
 */

public class Notification {

    public Hashtable<String,String> hashtable = new Hashtable<>();

    public String picture(){return hashtable.get("picture");}
    public String name(){return hashtable.get("name_of_creator") + " @" + hashtable.get("username_of_creator");}
    public String timestamp(){return hashtable.get("timestamp");}
    public String details(){return hashtable.get("details");}
    public String type(){return hashtable.get("type_of_notification");}
    public String id(){return hashtable.get("id");}

    public void setNotification(String data) throws JSONException {
        JSONObject json = new JSONObject(data);
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            hashtable.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
    }
    public boolean seen()
    {
        if(hashtable.get("seen").contains("true"))
        {
            Log.d("Seen","is true at " + details());
            return true;
        }
        return false;
    }
}
