package Classes;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by felix on 8/14/16.
 */
public class Micropost {
    // The Variables
    public Hashtable<String,String> micropostHash = new Hashtable<>();
    public String love_count,hate_count, comments_count,share_count;
    public boolean shared = false;
    public String creator_name;

    /* Getters */
    public String getContent(){
        return micropostHash.get("content").toString();
    }
    public String getTimeStamp(){
        return micropostHash.get("created_at").toString();
    }
    public String getId(){return micropostHash.get("id").toString();}
    public String getHateCount(){return hate_count;}
    public String getLoveCount(){return  love_count;}
    public String getCommentCount(){return comments_count;}
    public String getShareCount(){return share_count;}
    public String getImg(){
        return "";
    }
    public String getCreatorId(){return micropostHash.get("user_id");}
    public String getPureText(){return  micropostHash.get("pure_text");}

    public String getUrl() {
        return "https://www.goingvis.com/microposts/" + micropostHash.get("url_id");
    }


    public String getName(){
        return micropostHash.get("authors_full_name").toString() + " @"  + micropostHash.get("authors_username").toString();
    }

    public String getProfilePicture(){
        String pic = micropostHash.get("authors_picture").toString();
        if(pic.equals("default-user.png")){
            return "https://s3.amazonaws.com/goingvisphotos/website_photos/default-user.png";
        }else{
            return pic;
        }
    }

    /* Setter Methods */
    public void set_micropost (String data) throws JSONException {
        JSONObject json = new JSONObject(data);
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            micropostHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        // Init variables that will later change if modified.
        love_count = micropostHash.get("love_count");
        hate_count = micropostHash.get("hate_count");
        comments_count = micropostHash.get("comments_count");
        share_count = micropostHash.get("share_count");
    }


    public void set_with_hash_table(Hashtable<String,String> h)
    {
        this.micropostHash = h;
        love_count = micropostHash.get("love_count");
        hate_count = micropostHash.get("hate_count");
        comments_count = micropostHash.get("comments_count");
        share_count = micropostHash.get("share_count");
    }

    public void setWithPreSetHash()
    {
        love_count = micropostHash.get("love_count");
        hate_count = micropostHash.get("hate_count");
        comments_count = micropostHash.get("comments_count");
        share_count = micropostHash.get("share_count");
    }


    public String toString() {
        return micropostHash.toString();
    }
}
