package model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import Classes.Notification;

/**
 * Created by felix on 1/25/17.
 */

public class NotificationData {
    public List<Notification> notificationList = new ArrayList<>();

    public void setNotificationList(String output) throws JSONException {
        JSONArray json = new JSONArray(output);

        for (int i = 0; i < json.length(); i++) {
            final Notification m = new Notification();
            m.setNotification(json.getJSONObject(i).toString());
            notificationList.add(m);

        }
    }

}
