package model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import Classes.Product;

/**
 * Created by felix on 12/2/16.
 */

public class ProductData {
    List<Product> productList = new ArrayList<>();

    public void setProductList(String data) throws JSONException {
        JSONArray json = new JSONArray(data);

        for (int i = 0; i < json.length(); i++)
        {
            //JSONObject j = json.optJSONObject(Integer.parseInt(String.valueOf(i)));
            Log.d("Set Micropost at i: ", Integer.toString(i));
            final Product m = new Product();
            m.set_product(json.getJSONObject(i).toString());
            productList.add(m);
        }
    }

    public List<Product> getProductList(){return productList;}

}
