package model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Classes.DeepReply;

/**
 * Created by felix on 11/19/16.
 */

public class DeepReplyData {
    List<DeepReply> replyList = new ArrayList<>();
    int reply_id = 0;
    public boolean saved = false;

    public void setReplies(String data) throws JSONException {
        JSONArray json = new JSONArray(data);

        for (int i = 0; i < json.length(); i++) {
            //JSONObject j = json.optJSONObject(Integer.parseInt(String.valueOf(i)));
            DeepReply m = new DeepReply();
            m.set_deep_reply(json.getJSONObject(i).toString());
            replyList.add(m);
        }
        saved = true;
    }

    public void setRepliesHash(Hashtable<String,String> map){
        DeepReply reply = new DeepReply(map);
        replyList.add(0,reply);
    }

    public List<DeepReply> get_reply_list(){
        return replyList;
    }

    public void updateList(List<DeepReply> replies)
    {
        for (int i = 0; i < replies.size(); i++) {
            replies.add(replyList.get(i));
        }
    }
}