package model;

/**
 * Created by felix on 8/9/16.
 */
import android.app.Application;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;

import RestActions.GetUser;
import RestActions.SetNotificationToken;

public class UserData extends Application {
    User userClass;
    private String data;
    private int user_id;
    private String picture;
    public String username;
    private String name;
    private boolean seller;
    private String textToSend;
    private Hashtable<String, String> userHash = new Hashtable<String, String>();
    private boolean saved = false;
    private boolean canDelete = false;
    public String temp;
    public String positionTemp;
    public String [] follow_ids;
    public String [] friendsArray;
    public ArrayList<String> friends;
    public String currentConversation = null;
    public String inReply = "";
    public String inDeepReply = "";
    // Getters
    public String getData() {return data;}
    public int get_user_id() {return user_id;}
    public String picture(){return userHash.get("cropped_picture_url").toString();}
    public String username(){return username;}
    public String name(){return userHash.get("full_name").toString();}
    public String email(){return userHash.get("email").toString();}
    public String id(){return userHash.get("id").toString();}
    public String formattedName(){return(name() + " @" + username());}
    public String getNotificationID(){return userHash.get("notification_token");}
    public Hashtable get_user_hash(){return userHash;}
    public String textToSend(){return textToSend;}
    public void setTextToSend(String s){ this.textToSend = s;}
    public void setCanDelete(boolean b){this.canDelete = b;}
    public boolean getCanDelete(){return canDelete;}
    public boolean getSaved(){
        if (saved == true)
            return true;
        else
            return false;
    }


    //setters
    public void setData(String data) {this.data = data;}
    public void setCurrentConversation(String data){currentConversation = data;}
    public String getCurrentConversation(){
        if(currentConversation == null)
        {
            return "null";
        }
        return currentConversation;
    }
    // Sets the user data from the JSON Response
    public void setUser(String data,String notificationToken) throws JSONException {
        JSONObject json = new JSONObject(data);
        for(int i = 0; i<json.names().length(); i++){
            //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
            userHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
        setFollowers();

        // Sets notification token for this device.
        if(!userHash.get("notification_token").equalsIgnoreCase(notificationToken)){
            new SetNotificationToken().execute(userHash.get("username"),notificationToken);
        }
        username = userHash.get("username");
        new GetUser(new GetUser.AsyncResponse(){

            @Override
            public void processFinish(String output) throws JSONException {
                userClass = new User(output);
            }
        });
        saved = true;
    }

    public User userClass(){return userClass;}

    private void setFollowers()
    {
        String data = userHash.get("follow_ids");
        follow_ids = data.split(",");
    }

    public void removeFollower(String id)
    {
        ArrayList<String> list = new ArrayList<>();
        for(int i = 0; i < follow_ids.length; i++)
        {
            if(!follow_ids[i].equalsIgnoreCase(id))
            {
                list.add(follow_ids[i]);
            }
        }
        follow_ids = new String[list.size()];
        follow_ids = list.toArray(follow_ids);
    }

    public void addFollower(String id)
    {
        ArrayList<String> list = new ArrayList<>();
        for(int i = 0; i < follow_ids.length; i++)
        {
            list.add(follow_ids[i]);
        }
        list.add(id);
        follow_ids = new String[list.size()];
        follow_ids = list.toArray(follow_ids);
    }

    public boolean facebookActive()
    {
        if(userHash.get("name_facebook") != null && userHash.get("name_facebook").length() > 0 && !userHash.get("name_facebook").equalsIgnoreCase("null"))
        {
            Log.d("Facebook Active",userHash.get("name_facebook"));
            return true;
        }
        return false;
    }

    public boolean twitterActive()
    {
        if(userHash.get("twitter_username") != null && userHash.get("twitter_username").length() > 0 && !userHash.get("twitter_username").equalsIgnoreCase("null"))
        {
            return true;
        }
        return false;
    }

    public void setFriends(String data)
    {
        friends = new ArrayList<>();
        data = data.substring(1,data.length() - 1);
        data = data.substring(0,data.length() - 1);
        String [] splitByComma = data.split(",");
        for(int i = 0; i < splitByComma.length; i++)
        {
            friends.add(splitByComma[i].substring(1,splitByComma[i].length()  -1));
        }
        friendsArray = new String[friends.size()];
        friendsArray = friends.toArray(friendsArray);
    }

    public void setFriendsNewsFeed(String data)
    {
        friends = new ArrayList<>();
        data = data.substring(1,data.length() - 1);
        data = data.substring(0,data.length() - 1);
        String [] splitByComma = data.split(",");
        for(int i = 0; i < splitByComma.length; i++)
        {
            friends.add(splitByComma[i].substring(1,splitByComma[i].length()  -1).split(" ")[1]);
        }
        friendsArray = new String[friends.size()];
        friendsArray = friends.toArray(friendsArray);
    }

}