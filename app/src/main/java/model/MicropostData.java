package model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Classes.Micropost;
import RestActions.GetMicropost;

/**
 * Created by felix on 8/14/16.
 */
public class MicropostData {
    public List<Micropost> micropostList = new ArrayList<>();
    public boolean saved = false;

    public void setMicroposts(String data) throws JSONException {
        JSONArray json = new JSONArray(data);

        for (int i = 0; i < json.length(); i++)
        {
            //JSONObject j = json.optJSONObject(Integer.parseInt(String.valueOf(i)));
            Log.d("Set Micropost at i: ", Integer.toString(i));
            final Micropost m = new Micropost();
            m.set_micropost(json.getJSONObject(i).toString());
            if (!m.micropostHash.get("original_user").equalsIgnoreCase("null") )
            {
                GetMicropost gp = (GetMicropost) new GetMicropost(new GetMicropost.AsyncResponse(){
                    @Override
                    public void processFinish(String output) throws JSONException {
                        Micropost newM = new Micropost();
                        newM.set_micropost(output);
                        newM.creator_name =  m.micropostHash.get("authors_full_name") + " @" + m.micropostHash.get("authors_username");
                        newM.shared = true;
                        micropostList.add(newM);
                    }
                }).execute(m.micropostHash.get("share_id"));
            }
            else
            {
                micropostList.add(m);
            }
        }
        saved = true;
    }

    public void setSpecificMicropost(String output) throws JSONException
    {
        JSONObject json = new JSONObject(output);
        final Micropost m = new Micropost();
        for(int i = 0; i<json.names().length(); i++) {
            m.micropostHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
        }
            m.setWithPreSetHash();
            if (!m.micropostHash.get("original_user").equalsIgnoreCase("null") )
            {
                GetMicropost gp = (GetMicropost) new GetMicropost(new GetMicropost.AsyncResponse(){
                    @Override
                    public void processFinish(String output) throws JSONException {
                        Micropost newM = new Micropost();
                        newM.set_micropost(output);
                        newM.creator_name =  m.micropostHash.get("authors_full_name") + " @" + m.micropostHash.get("authors_username");
                        newM.shared = true;
                        micropostList.add(newM);
                    }
                }).execute(m.micropostHash.get("share_id"));
            }
            else
            {
                micropostList.add(m);
            }
    }

    public void prependMicropostWithHash(Hashtable<String,String> microHash) throws JSONException
    {
        Micropost m = new Micropost();
        m.set_with_hash_table(microHash);
        micropostList.add(0,m); // Add to the start
        saved = true;
    }
    public List<Micropost> get_micropost_list(){
        return micropostList;
    }

    public void updateList(List<Micropost> microposts)
    {
        for (int i = 0; i < microposts.size(); i++) {
            microposts.add(micropostList.get(i));
        }
    }
}
