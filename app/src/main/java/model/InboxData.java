package model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import Classes.InboxItem;

/**
 * Created by felix on 12/6/16.
 */

public class InboxData {
    public List<InboxItem > inboxList = new ArrayList<>();

    public void setInbox(String data) throws JSONException {
        JSONArray json = new JSONArray(data);

        for (int i = 0; i < json.length(); i++)
        {
            //JSONObject j = json.optJSONObject(Integer.parseInt(String.valueOf(i)));
            Log.d("Set Micropost at i: ", Integer.toString(i));
            final InboxItem m = new InboxItem();
            m.set_inbox_item(json.getJSONObject(i).toString());
            inboxList.add(m);
        }
    }
}
