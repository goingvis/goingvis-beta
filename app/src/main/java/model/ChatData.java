package model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Classes.ChatItem;

/**
 * Created by felix on 12/11/16.
 */

public class ChatData {
    public List<ChatItem> chatList = new ArrayList<>();

    public void setConversation(String data) throws JSONException {
        JSONArray json = new JSONArray(data);

        for (int i = 0; i < json.length(); i++)
        {
            //JSONObject j = json.optJSONObject(Integer.parseInt(String.valueOf(i)));
            Log.d("Set Micropost at i: ", Integer.toString(i));
            final ChatItem m = new ChatItem();
            m.set_chat_item(json.getJSONObject(i).toString());
            chatList.add(m);
        }
    }

    public void setWithHash(Hashtable<String,String> table)
    {
        ChatItem m = new ChatItem();
        m.set_chat_item_hash(table);
        chatList.add(m);
    }
}
