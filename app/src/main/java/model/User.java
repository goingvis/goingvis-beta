package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by felix on 11/19/16.
 */

public class User  {
        private String data;
        private int user_id;
        private String picture;
        private String username;
        private String name;
        private boolean seller;
        private String textToSend;
        public Hashtable<String, String> userHash = new Hashtable<String, String>();
        private boolean saved = false;
        private boolean canDelete = false;
        public String temp;
        public String positionTemp;
        public String [] follow_ids;
        // Getters
        public String getData() {return data;}
        public int get_user_id() {return user_id;}
        public String picture(){return userHash.get("cropped_picture_url").toString();}
        public String username(){return userHash.get("username").toString();}
        public String name(){return userHash.get("full_name").toString();}
        public String email(){return userHash.get("email").toString();}
        public String id(){return userHash.get("id").toString();}
        public String formattedName(){return(name() + " @" + username());}
        public Hashtable get_user_hash(){return userHash;}
        public String textToSend(){return textToSend;}
        public void setTextToSend(String s){ this.textToSend = s;}
        public void setCanDelete(boolean b){this.canDelete = b;}
        public boolean getCanDelete(){return canDelete;}
        public boolean getSaved(){
            if (saved == true)
                return true;
            else
                return false;
        }


        public User(String data) throws JSONException {
            setUser(data);
        }

        public void setData(String data) {this.data = data;}
        // Sets the user data from the JSON Response
        public void setUser(String data) throws JSONException {
            JSONObject json = new JSONObject(data);
            for(int i = 0; i<json.names().length(); i++){
                //Log.v("HELLO", "key = " + json.names().getString(i) + " value = " + json.get(json.names().getString(i)));
                userHash.put(json.names().getString(i), json.get(json.names().getString(i)).toString());
            }
            setFollowers();
            saved = true;
        }

        private void setFollowers()
        {
            String data = userHash.get("follow_ids");
            follow_ids = data.split(",");
        }

}