package model;

import Classes.Utility;

/**
 * Created by felix on 11/1/16.
 */

public class LinkPreview {
    public String title;
    public String url;
    public String type;
    public String picture;
    public String description;
    public boolean youtube = false;
    public boolean link = false;
    public String rootUrl;


    public LinkPreview(String title, String url, String type, String picture, String description)
    {
        this.title = title;
        this.url = url;
        this.type = type;
        this.picture = picture;
        this.description = description;
        this.rootUrl = Utility.findRootUrl(url);
    }

}
