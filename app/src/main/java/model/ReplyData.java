package model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Classes.Reply;

/**
 * Created by felix on 10/28/16.
 */

public class ReplyData {
    List<Reply> replyList = new ArrayList<>();
    int micropost_id = 0;
    public boolean saved = false;

    public void setReplies(String data) throws JSONException {
        JSONArray json = new JSONArray(data);

        for (int i = 0; i < json.length(); i++) {
            //JSONObject j = json.optJSONObject(Integer.parseInt(String.valueOf(i)));
            Reply m = new Reply();
            m.set_reply(json.getJSONObject(i).toString());
            replyList.add(m);
        }
        saved = true;
    }

    public void setRepliesHash(Hashtable<String,String> map){
        Reply reply = new Reply(map);
        replyList.add(0,reply);
    }

    public List<Reply> get_reply_list(){
        return replyList;
    }

    public void updateList(List<Reply> replies)
    {
        for (int i = 0; i < replies.size(); i++) {
            replies.add(replyList.get(i));
        }
    }
}