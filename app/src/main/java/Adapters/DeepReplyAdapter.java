package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.felix.goingvis_prototype.R;

import java.util.List;

import Classes.DeepReply;
import Classes.VolleySingleton;
import Helpers.DeepReplyDataStore;
import model.UserData;
import RestActions.Hate;
import RestActions.Love;

/**
 * Created by felix on 11/19/16.
 */

public class DeepReplyAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DeepReply> feedItems;
    ImageLoader mImageLoader;
    View feedItemLayout;
    Application applicationContext;
    private String userId;
    public View rootView;
    public TextView love_count;
    public TextView hate_count;
    public TextView comment_count;
    DeepReplyDataStore deepReplyDataStore;
    List<DeepReply> deepReplies;
    DeepReplyAdapter deepReplyAdapter;
    String username;

    public DeepReplyAdapter(Activity activity, List<DeepReply> feedItems, View layout, DeepReplyDataStore deepReplyDataStore) {
        this.activity = activity;
        this.feedItems = feedItems;
        this.feedItemLayout = layout;
        this.deepReplyDataStore = deepReplyDataStore;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }


    public void setApplicationContext(Application application) {
        this.applicationContext = application;
        UserData user = (UserData) application.getApplicationContext();
        userId = user.id();
        username = user.username();

    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.reply_item, null);
            rootView = convertView;
            //convertView = feedItemLayout;
        }


        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView statusMsg = (TextView) convertView
                .findViewById(R.id.txtStatusMsg);
        TextView url = (TextView) convertView.findViewById(R.id.txtUrl);
        NetworkImageView profilePic = (NetworkImageView) convertView.findViewById(R.id.profilePic);
        // FeedImageView feedImageView = (FeedImageView) convertView
        //       .findViewById(R.id.feedImage1);

        DeepReply item = feedItems.get(position);

        name.setText(item.getName());

        // Converting timestamp into x ago format
        // CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
        //        Long.parseLong(item.getTimeStamp()),
        //        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        timestamp.setText(item.getTimeStamp());

        // Chcek for empty status message
        if (!TextUtils.isEmpty(item.getContent())) {
            statusMsg.setText(Html.fromHtml(item.getContent()));
            statusMsg.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            statusMsg.setVisibility(View.GONE);
        }


        VolleySingleton.setApplication(applicationContext);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        profilePic.setImageUrl(item.getProfilePicture(), mImageLoader);

        //////// Replies and Deep Replies Setup ////////////////
        final ImageView love_icon = (ImageView) convertView.findViewById(R.id.love_icon);
        ImageView hate_icon = (ImageView) convertView.findViewById(R.id.hate_icon);
        ImageView comment_icon = (ImageView) convertView.findViewById(R.id.comment_icon);
        // Texts
        love_count = (TextView) convertView.findViewById(R.id.love_count);
        hate_count = (TextView) convertView.findViewById(R.id.hate_count);
        comment_count = (TextView) convertView.findViewById(R.id.comment_count);
        // Setting tags for Icons
        love_icon.setTag("deep_reply_" + item.getId());
        hate_icon.setTag("deep_reply_" + item.getId());
        comment_icon.setTag("deep_reply_" + item.getId());
        // Set tags for counts
        love_count.setText(item.getLoveCount());
        hate_count.setText(item.getHateCount());
        comment_count.setText("");
        love_count.setTag("love_deep_reply_" + item.getId());
        hate_count.setTag("hate_deep_reply_" + item.getId());
        comment_count.setTag("comment_deep_reply_" + item.getId());

        set_click_actions(love_icon, hate_icon, comment_icon);

        return convertView;
    }

    void set_click_actions(final ImageView love, final ImageView hate, final ImageView comment) {
        //Love click action
        love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Love l = new Love();
                String[] id = love.getTag().toString().split("_");
                Log.d("ID: ", id[1]);
                l.execute("deep_reply", id[2].toString(), userId);
                Toast.makeText(applicationContext,love.getTag().toString(),Toast.LENGTH_LONG);
            }
        });

        //Love click action
        hate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hate l = new Hate();
                String[] id = hate.getTag().toString().split("_");
                Log.d("ID: ", id[1]);
                l.execute("deep_reply", id[2].toString(), userId);
            }
        });

        //Love click action
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final String[] id = comment.getTag().toString().split("_");
            }
        });

    }
}

