package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.felix.goingvis_prototype.R;

import Classes.VolleySingleton;

/**
 * Created by felix on 12/2/16.
 */

public class GalleryAdapter extends PagerAdapter {
    Activity activity;
    LayoutInflater inflater;
    String [] images;
    ImageLoader imageLoader;
    Application applicationContext;

    public GalleryAdapter(Activity activity, String[] images, Application application){
        this.activity = activity;
        this.images = images;
        applicationContext = application;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = inflater.inflate(R.layout.gallery_item, null);

        VolleySingleton.setApplication(applicationContext);
        imageLoader = VolleySingleton.getInstance().getImageLoader();

        NetworkImageView imageView = (NetworkImageView) convertView.findViewById(R.id.galleryImage);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        imageView.setMinimumWidth(width);
        imageView.setImageUrl(images[position], imageLoader);
        Log.d("IMAGE SET",images[position]);
        container.addView(convertView);

        return convertView;
    }
}
