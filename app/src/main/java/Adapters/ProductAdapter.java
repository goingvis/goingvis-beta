package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.felix.goingvis_prototype.R;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Classes.Product;
import Classes.Utility;
import Classes.VolleySingleton;
import model.UserData;

/**
 * Created by felix on 11/30/16.
 */

public class ProductAdapter extends BaseAdapter {
    Activity activity;
    List<Product> listItem;
    public Hashtable<String,String> productHash = new Hashtable<>();
    Application applicationContext;
    UserData user;
    private LayoutInflater inflater;
    private Product item;
    ImageLoader mImageLoader;




    ProductAdapter(){}
    public ProductAdapter(Activity activity, List<Product> products){
        this.activity = activity;
        this.listItem = products;
    }

    public void setApplicationContext(Application application){
        this.applicationContext = application;
        user = (UserData) application.getApplicationContext();
    }


    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.product_item, null);
            //convertView = feedItemLayout;
        }

        item = listItem.get(position);
        VolleySingleton.setApplication(applicationContext);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

        NetworkImageView picture = (NetworkImageView) convertView.findViewById(R.id.productPic);
        TextView name = (TextView) convertView.findViewById(R.id.productName);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        picture.setMinimumWidth(width);
        ArrayList<String> stars = Utility.setStarsProduct(item);
        Utility.setBackgroundStar(stars,convertView);
        name.setText(item.productHash.get("name"));
        try {
            picture.setImageUrl(Utility.setProductPicture(item),mImageLoader);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }
}
