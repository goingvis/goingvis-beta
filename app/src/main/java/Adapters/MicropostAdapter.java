package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.felix.goingvis_prototype.FullScreenImageViewActivity;
import com.example.felix.goingvis_prototype.R;
import com.example.felix.goingvis_prototype.WebviewActivity;
import com.example.felix.goingvis_prototype.YtPlayActivity;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import Classes.Micropost;
import Classes.Reply;
import Classes.Utility;
import Classes.VolleySingleton;
import Dialogs.MicropostOptions;
import Helpers.DeepReplyDataStore;
import Helpers.ReplyDataStore;
import Helpers.YouTubeHelper;
import RestActions.CreateShare;
import RestActions.Follow;
import RestActions.GetReplies;
import RestActions.GetUserFeed;
import RestActions.Hate;
import RestActions.Love;
import RestActions.NewFeedReply;
import model.LinkPreview;
import model.ReplyData;
import model.User;
import model.UserData;

/**
 * Created by felix on 8/15/16.
 */
public class MicropostAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Micropost> feedItems;
    List<Reply> replies;
    ImageLoader mImageLoader;
    View feedItemLayout;
    Application applicationContext;
    private String userId;
    private String username;
    String micropost_id;
    ReplyAdapter replyAdapter;
    DeepReplyDataStore deepReplyDataStore;
    LinkPreview preview;
    View linkPreviewView;
    ReplyDataStore replyDataStore;
    Micropost item;
    UserData user;
    String textToSend;
    String micropost_creator;
    TextView love_count,hate_count,comment_count,share_count;
    String userNewsFeed;
    String specificMicropost;
    User nonUser = null;
    String urlToVisit = null;

    public MicropostAdapter(Activity activity, List<Micropost> feedItems,View layout, ReplyDataStore rep,DeepReplyDataStore dep,String user,User nonUser,String sp)
    {
        this.activity = activity;
        this.feedItems = feedItems;
        this.feedItemLayout = layout;
        this.replyDataStore = rep;
        this.deepReplyDataStore = dep;
        this.userNewsFeed = user;
        this.nonUser = nonUser;
        this.specificMicropost = sp;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    public void setApplicationContext(Application application){
        this.applicationContext = application;
        user = (UserData) application.getApplicationContext();
        userId = user.id();
        username = user.username();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.feed_item, null);
            //convertView = feedItemLayout;
        }

        item = feedItems.get(position);
        VolleySingleton.setApplication(applicationContext);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        // Check if we need to remove non user profile card.
        if(position == 0)
        {

            if(!user.username().equalsIgnoreCase(userNewsFeed)) {
                //View userCard = inflater.inflate(R.layout.news_feed_user_card, null);
                set_profile_card(convertView);

                //ViewGroup vv = (ViewGroup) convertView.findViewById(R.id.newsFeedItem);
                //vv.addView(userCard);
               convertView.findViewById(R.id.nonUserProfileCard).setVisibility(View.VISIBLE);
            }

        }
        else
        {
            convertView.findViewById(R.id.nonUserProfileCard).setVisibility(View.GONE);
        }
        // Creat micropost content
        setMicropost(convertView,item,position);
        convertView.setVisibility(View.VISIBLE);
        return convertView;
    }

    /* Function Sets Profile card if the news feed is another user */
    void set_profile_card(View convertView)
    {
        // Init views
        NetworkImageView userPic = (NetworkImageView) convertView.findViewById(R.id.nonUserProfileCardPicture);
        TextView fullName = (TextView) convertView.findViewById(R.id.nonUserProfileCardFullName);
        TextView userName = (TextView) convertView.findViewById(R.id.nonUserProfileCardUsername);
        final Button followAndUnfollow = (Button) convertView.findViewById(R.id.followButton);

        Log.e("set_profile_card",nonUser.name());
        userPic.setImageUrl(nonUser.picture(),mImageLoader);
        fullName.setText(nonUser.name());
        ArrayList<String> stars = Utility.setStars(nonUser);
        if(stars == null)
        {
            View s = (View) convertView.findViewById(R.id.nonUserProfileCardRating);
            s.setVisibility(View.GONE);
        }
        else
        {
            Utility.setBackgroundStar(stars,convertView);
        }

        userName.setText(" @"  + nonUser.username());

        // Set Follow and unfollow
        followAndUnfollow.setText(Utility.followOrUnfollow(user,nonUser));
        followAndUnfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String type;
                String after;
                if(followAndUnfollow.getText().toString().equalsIgnoreCase("Follow"))
                {
                    type = "follow";
                    after = "Unfollow";
                    user.addFollower(nonUser.id());
                }
                else
                {
                    type = "unfollow";
                    after = "Follow";
                    user.removeFollower(nonUser.id());
                }
                new Follow(new Follow.AsyncResponse() {

                    @Override
                    public void processFinish(String output) throws JSONException {

                    }
                }).execute(type,user.id(),nonUser.id());
                followAndUnfollow.setText(after);
            }
        });
    }


    /* Sets Micropost views and logic */
    void setMicropost(View convertView, final Micropost item, int position)
    {
        final TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView statusMsg = (TextView) convertView
                .findViewById(R.id.txtStatusMsg);
//        TextView url = (TextView) convertView.findViewById(R.id.txtUrl);
        NetworkImageView profilePic = (NetworkImageView) convertView.findViewById(R.id.profilePic);

        if(item.shared)
        {
            name.setText(Utility.newsFeedContentExtras(item.creator_name + " Shared " + item.getName(),activity));
        }
        else
        {
            name.setText(Utility.newsFeedContentExtras(item.getName(),activity));
        }
        name.setMovementMethod(LinkMovementMethod.getInstance());
        micropost_id = item.getId();
        textToSend = item.getUrl();
        micropost_creator = item.getCreatorId();

        timestamp.setText(item.getTimeStamp());

        // Check for empty status message
        if (!TextUtils.isEmpty(item.getPureText()))
        {
            statusMsg.setText(Utility.newsFeedContentExtras(item.getPureText(),activity));
            statusMsg.setMovementMethod(LinkMovementMethod.getInstance());
            statusMsg.setVisibility(View.VISIBLE);
        }
        else
        {
            statusMsg.setVisibility(View.GONE);
        }

        // user profile pic

        profilePic.setImageUrl(item.getProfilePicture(), mImageLoader);

        //////// Replies and Deep Replies Setup ////////////////
        final ImageView love_icon = (ImageView) convertView.findViewById(R.id.love_icon);
        ImageView hate_icon = (ImageView) convertView.findViewById(R.id.hate_icon);
        ImageView comment_icon = (ImageView) convertView.findViewById(R.id.comment_icon);
        ImageView share_icon = (ImageView) convertView.findViewById(R.id.share_icon);
        ImageView elip_icon = (ImageView) convertView.findViewById(R.id.elip);
        elip_icon.setTag(item.getId() + "-" + item.micropostHash.get("authors_username") + "-" +
                position + "-" + item.micropostHash.get("url_id"));
        // Texts
        love_count = (TextView) convertView.findViewById(R.id.love_count);
        hate_count = (TextView) convertView.findViewById(R.id.hate_count);
        comment_count = (TextView) convertView.findViewById(R.id.comment_count);
        share_count = (TextView) convertView.findViewById(R.id.share_count);
        // Setting tags for Icons
        love_icon.setTag("micropost_" + item.getId());
        hate_icon.setTag("micropost_" + item.getId());
        comment_icon.setTag("micropost_" + item.getId());
        share_icon.setTag("micropost_" + item.getId());
        // Set tags for counts
        love_count.setText(item.getLoveCount());
        hate_count.setText(item.getHateCount());
        comment_count.setText(item.getCommentCount());
        share_count.setText(item.getShareCount());
        love_count.setTag("love_micropost_" + item.getId());
        hate_count.setTag("hate_micropost_" + item.getId());
        comment_count.setTag("comment_micropost_" + item.getId());
        share_count.setTag("share_micropost_" + item.getId());

        // Link Previews for youtube, links and such
        String extra = Utility.contentExtrasMicropost(item);
        Boolean ex = false;
        if(extra != null && !extra.equalsIgnoreCase("null"))
        {
            ex = true;
        }
        if(ex)
        {
            preview = null;
            try {
                preview = Utility.contentExtrasMicropostLink(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(preview != null) {
                TextView title = (TextView) convertView.findViewById(R.id.link_title);
                title.setText(preview.title);
                title.setVisibility(View.VISIBLE);

                TextView desc = (TextView) convertView.findViewById(R.id.link_description);
                desc.setText(preview.description);
                desc.setVisibility(View.VISIBLE);

                NetworkImageView imagePreview = (NetworkImageView) convertView.findViewById(R.id.link_image);
                imagePreview.setImageUrl(preview.picture, mImageLoader);
                imagePreview.setVisibility(View.VISIBLE);

                linkPreviewView = (View) convertView.findViewById(R.id.link_preview);
                linkPreviewView.setTag(preview.url + "-" + item.getId());
                // SAVE PREVIEW URL TO THE LOVE ICON. WORKAROUND....
                love_icon.setTag( love_icon.getTag().toString() + "_" + preview.url );

                // Remove or Keep play button
                ImageView im = (ImageView) convertView.findViewById(R.id.playButton);

                if(!preview.youtube){
                    im.setVisibility(View.GONE);
                }

                // Set root link
                TextView rootLink = (TextView) convertView.findViewById(R.id.rootLink);
                rootLink.setText(preview.rootUrl);
                linkPreviewView.setVisibility(View.VISIBLE);
            }
            else{
                linkPreviewView = (View) convertView.findViewById(R.id.link_preview);
                linkPreviewView.setVisibility(View.GONE);
            }

        }
        else
        {
            linkPreviewView = (View) convertView.findViewById(R.id.link_preview);
            linkPreviewView.setVisibility(View.GONE);
        }

        // Check for regular image upload!
        NetworkImageView microPic = (NetworkImageView) convertView.findViewById(R.id.micropostPicture);
        if(!item.micropostHash.get("picture").equalsIgnoreCase("none")){
            microPic.setImageUrl(item.micropostHash.get("picture"), mImageLoader);
            microPic.setVisibility(View.VISIBLE);
            linkPreviewView.setVisibility(View.GONE);

            microPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity,FullScreenImageViewActivity.class);
                    i.putExtra("title",Utility.newsFeedContentExtras(item.getPureText(),activity));
                    i.putExtra("photo_url",item.micropostHash.get("picture").replaceAll(" ", "%20"));
                    activity.startActivity(i);
                }
            });

        }else{
            microPic.setVisibility(View.GONE);
        }

        // For a specific micropost the margin changes
        if(specificMicropost.length() > 0) {
            View feed = convertView.findViewById(R.id.newsFeedLayout);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(0,0,0,0);
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            feed.setLayoutParams(params);
        }

        set_click_actions(love_icon,hate_icon,comment_icon,share_icon,elip_icon);
    }

    void set_click_actions(final ImageView love, final ImageView hate, final ImageView comment, final ImageView share, final ImageView elip)
    {
        //Love click action
        love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Love l = new Love();
                String[] id = love.getTag().toString().split("_");
                l.execute("micropost",id[1].toString(),userId);
            }
        });

        //Love click action
        hate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hate l = new Hate();
                String[] id = hate.getTag().toString().split("_");
                l.execute("micropost",id[1].toString(),userId);
            }
        });

        //Love click action
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final String[] id = comment.getTag().toString().split("_");
                GetReplies gp = (GetReplies) new GetReplies(new GetReplies.AsyncResponse(){
                    @Override
                    public void processFinish(String output) throws JSONException {
                        onShowPopupReply(v,output,id[1].toString());
                    }
                }).execute(id[1].toString());
            }
        });

        //Love click action
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String id = share.getTag().toString();
                CreateShare asyncTask = (CreateShare) new CreateShare(new CreateShare.AsyncResponse(){

                    @Override
                    public void processFinish(String output) throws JSONException {
                        item.share_count = String.valueOf( Integer.parseInt(item.share_count) + 1 );
                        share_count.setText(item.getShareCount());
                    }
                }).execute(user.id(),id.split("_")[1]);

            }
        });

        elip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setTextToSend(textToSend);
                user.temp = elip.getTag().toString().split("-")[0];
                Log.d("Other username",elip.getTag().toString().split("-")[1]);
                Log.d("my username",user.id());
                if( elip.getTag().toString().split("-")[1].equalsIgnoreCase(user.username) )
                {
                    user.positionTemp = elip.getTag().toString().split("-")[2];
                    user.setCanDelete(true);
                }
                else
                {
                    user.setCanDelete(false);
                }
                MicropostOptions dialogFragment = new
                        MicropostOptions(Utility.createLink("microposts",elip.getTag().toString().split("-")[3]));
                dialogFragment.show(activity.getFragmentManager(),"Micropost Options");
            }
        });

        if(preview != null)
        {
            Log.e("preview","not null");
            linkPreviewView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Get preview url that has been saved to the love icon.
                    String theUrl = love.getTag().toString().split("_")[2];
                    Log.e("preview onClick", theUrl);
                    goToUrl( theUrl );
                }
            });
        }
    }

    public void onShowPopupReply(View v, String output, final String myID) throws JSONException
    {
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // inflate the custom popup layout
        View inflatedView = inflater.inflate(R.layout.popup_layout, null,false);
        // find the ListView in the popup layout
        ListView listView = (ListView)inflatedView.findViewById(R.id.commentsListView);
        // get device size
        WindowManager wm = (WindowManager)applicationContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;
        DisplayMetrics displayMetrics = applicationContext.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // Setting up ReplyList Adapter
        ReplyData replyData = new ReplyData();
        replyData.setReplies(output);
        replies = replyData.get_reply_list();
        replyAdapter = new ReplyAdapter(activity, replies,listView,deepReplyDataStore,v);
        replyAdapter.setApplicationContext(applicationContext);

        listView.setAdapter(replyAdapter);
        replyAdapter.notifyDataSetChanged();

        // Reply Data Storage to be accessed in NewFeed
        replyDataStore.add(replyAdapter,replyData, myID,inflatedView);

        final TextView replyInput = (TextView) inflatedView.findViewById(R.id.replyInput);
        final Button replyButton = (Button) inflatedView.findViewById(R.id.replyButton);
        replyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replyInput.setEnabled(false);
                replyButton.setEnabled(false);
                NewFeedReply asyncTask = (NewFeedReply) new NewFeedReply(new GetUserFeed.AsyncResponse(){

                    @Override
                    public void processFinish(String output) throws JSONException {
                        replyButton.setEnabled(true);
                        replyInput.setEnabled(true);
                        //progressBar.setVisibility(View.GONE);
                        //getDialog().dismiss();
                    }
                }).execute(replyInput.getText().toString(),myID,username);
                replyInput.setText("");
            }
        });



        // fill the data to the list items
       // setSimpleList(listView);


        // set height depends on the device size
        PopupWindow popWindow = new PopupWindow(inflatedView, width,height-50, true );
        // set a background drawable with rounders corners
        popWindow.setBackgroundDrawable(applicationContext.getResources().getDrawable(R.drawable.popup_bg));

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(v, Gravity.BOTTOM, 0,100);
    }

    private void goToUrl (String url) {
        Log.e("goToUrl",url);
        if(url.contains("youtube.com") || url.contains("youtu.be")){
            Intent intent = new Intent(activity, YtPlayActivity.class);
            String id = YouTubeHelper.extractVideoIdFromUrl(url);
            intent.putExtra("PId", id);
            activity.startActivity(intent);
        }else{
            Intent mWebviewActivity = new Intent(activity,WebviewActivity.class);
            mWebviewActivity.putExtra("linkUrl",url);
            mWebviewActivity.putExtra("title",Utility.newsFeedContentExtras(item.getPureText(),activity));
            activity.startActivity(mWebviewActivity);

            /*Uri uriUrl = Uri.parse(url);
            Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
            launchBrowser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(launchBrowser);*/
        }
    }

}