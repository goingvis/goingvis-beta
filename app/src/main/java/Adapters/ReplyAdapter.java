package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.felix.goingvis_prototype.R;

import org.json.JSONException;

import java.util.List;

import Classes.DeepReply;
import Classes.Reply;
import Classes.VolleySingleton;
import Helpers.DeepReplyDataStore;
import model.DeepReplyData;
import model.UserData;
import RestActions.GetDeepReplies;
import RestActions.Hate;
import RestActions.Love;
import RestActions.NewFeedDeepReply;

/**
 * Created by felix on 10/25/16.
 */

public class ReplyAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Reply> feedItems;
    ImageLoader mImageLoader;
    View feedItemLayout;
    Application applicationContext;
    private String userId;
    public View rootView;
    public TextView love_count;
    public TextView hate_count;
    public TextView comment_count;
    DeepReplyDataStore deepReplyDataStore;
    List<DeepReply> deepReplies;
    DeepReplyAdapter deepReplyAdapter;
    String username;
    View replyView;

    public ReplyAdapter(Activity activity, List<Reply> feedItems, View layout, DeepReplyDataStore deepReplyDataStore, View replyV) {
        this.activity = activity;
        this.feedItems = feedItems;
        this.feedItemLayout = layout;
        this.deepReplyDataStore = deepReplyDataStore;
        this.replyView = replyV;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }


    public void setApplicationContext(Application application) {
        this.applicationContext = application;
        UserData user = (UserData) application.getApplicationContext();
        userId = user.id();
        username = user.username();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.reply_item, null);
            rootView = convertView;
            //convertView = feedItemLayout;
        }


        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView statusMsg = (TextView) convertView
                .findViewById(R.id.txtStatusMsg);
        TextView url = (TextView) convertView.findViewById(R.id.txtUrl);
        NetworkImageView profilePic = (NetworkImageView) convertView.findViewById(R.id.profilePic);
        // FeedImageView feedImageView = (FeedImageView) convertView
        //       .findViewById(R.id.feedImage1);

        Reply item = feedItems.get(position);

        name.setText(item.getName());

        // Converting timestamp into x ago format
        // CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
        //        Long.parseLong(item.getTimeStamp()),
        //        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        timestamp.setText(item.getTimeStamp());

        // Chcek for empty status message
        if (!TextUtils.isEmpty(item.getContent())) {
            statusMsg.setText(Html.fromHtml(item.getContent()));
            statusMsg.setVisibility(View.VISIBLE);
        } else {
            // status is empty, remove from view
            statusMsg.setVisibility(View.GONE);
        }

        // Checking for null feed url
        /*
        if (item.getUrl() != null) {
            url.setText(Html.fromHtml("<a href=\"" + item.getUrl() + "\">"
                    + item.getUrl() + "</a> "));

            // Making url clickable
            url.setMovementMethod(LinkMovementMethod.getInstance());
            url.setVisibility(View.VISIBLE);
        } else {
            // url is null, remove from the view
            url.setVisibility(View.GONE);
        }
        */

        // user profile pic

        VolleySingleton.setApplication(applicationContext);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();
        profilePic.setImageUrl(item.getProfilePicture(), mImageLoader);

        //////// Replies and Deep Replies Setup ////////////////
        final ImageView love_icon = (ImageView) convertView.findViewById(R.id.love_icon);
        ImageView hate_icon = (ImageView) convertView.findViewById(R.id.hate_icon);
        ImageView comment_icon = (ImageView) convertView.findViewById(R.id.comment_icon);
        // Texts
        love_count = (TextView) convertView.findViewById(R.id.love_count);
        hate_count = (TextView) convertView.findViewById(R.id.hate_count);
        comment_count = (TextView) convertView.findViewById(R.id.comment_count);
        // Setting tags for Icons
        love_icon.setTag("reply_" + item.getId());
        hate_icon.setTag("reply_" + item.getId());
        comment_icon.setTag("reply_" + item.getId());
        // Set tags for counts
        love_count.setText(item.getLoveCount());
        hate_count.setText(item.getHateCount());
        comment_count.setText(item.getCommentCount());
        love_count.setTag("love_reply_" + item.getId());
        hate_count.setTag("hate_reply_" + item.getId());
        comment_count.setTag("comment_reply_" + item.getId());

        set_click_actions(love_icon, hate_icon, comment_icon);

        return convertView;
    }

    void set_click_actions(final ImageView love, final ImageView hate, final ImageView comment) {
        //Love click action
        love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Love l = new Love();
                String[] id = love.getTag().toString().split("_");
                Log.d("ID: ", id[1]);
                l.execute("reply", id[1].toString(), userId);
                Toast.makeText(applicationContext,love.getTag().toString(),Toast.LENGTH_LONG);
            }
        });

        //Love click action
        hate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hate l = new Hate();
                String[] id = hate.getTag().toString().split("_");
                Log.d("ID: ", id[1]);
                l.execute("reply", id[1].toString(), userId);
            }
        });

        //Love click action
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final String[] id = comment.getTag().toString().split("_");
                GetDeepReplies gp = (GetDeepReplies) new GetDeepReplies(new GetDeepReplies.AsyncResponse(){
                    @Override
                    public void processFinish(String output) throws JSONException {
                        onShowPopupReply(v,output,id[1].toString());
                    }
                }).execute(id[1].toString());
            }
        });

    }

    public void onShowPopupReply(View v, String output, final String myID) throws JSONException {

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // inflate the custom popup layout
        View inflatedView = inflater.inflate(R.layout.popup_layout, null,false);
        // find the ListView in the popup layout
        ListView listView = (ListView)inflatedView.findViewById(R.id.commentsListView);
        // get device size
        WindowManager wm = (WindowManager)applicationContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;
        DisplayMetrics displayMetrics = applicationContext.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        // Setting up ReplyList Adapter
        DeepReplyData replyData = new DeepReplyData();
        replyData.setReplies(output);
        deepReplies = replyData.get_reply_list();
        deepReplyAdapter = new DeepReplyAdapter(activity, deepReplies,listView,deepReplyDataStore);
        deepReplyAdapter.setApplicationContext(applicationContext);

        listView.setAdapter(deepReplyAdapter);
        deepReplyAdapter.notifyDataSetChanged();

        // Reply Data Storage to be accessed in NewFeed
        deepReplyDataStore.add(deepReplyAdapter,replyData, myID,inflatedView);

        final TextView replyInput = (TextView) inflatedView.findViewById(R.id.replyInput);
        final Button replyButton = (Button) inflatedView.findViewById(R.id.replyButton);
        replyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replyInput.setEnabled(false);
                replyButton.setEnabled(false);
                NewFeedDeepReply asyncTask = (NewFeedDeepReply) new NewFeedDeepReply(new NewFeedDeepReply.AsyncResponse(){

                    @Override
                    public void processFinish(String output) throws JSONException {
                        replyButton.setEnabled(true);
                        replyInput.setEnabled(true);
                    }
                }).execute(replyInput.getText().toString(),myID,username);
                replyInput.setText("");
            }
        });



        // fill the data to the list items
        // setSimpleList(listView);


        // set height depends on the device size
        PopupWindow popWindow = new PopupWindow(inflatedView, width,height-50, true );
        // set a background drawable with rounders corners
        popWindow.setBackgroundDrawable(applicationContext.getResources().getDrawable(R.drawable.popup_bg));

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(replyView, Gravity.BOTTOM, 0,100);
    }

}

