package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.felix.goingvis_prototype.R;

import java.util.List;

import Classes.InboxItem;
import Classes.VolleySingleton;
import model.UserData;

/**
 * Created by felix on 12/6/16.
 */

public class InboxAdapter extends BaseAdapter {

    UserData user;
    LayoutInflater inflater;
    Application applicationContext;
    Activity activity;
    private List<InboxItem> inboxItems;
    InboxItem item;
    ImageLoader imageLoader;


    InboxAdapter(){}
    public InboxAdapter(Activity activity, List<InboxItem> inboxItems, Application application){
        this.activity = activity;
        applicationContext = application;
        this.inboxItems = inboxItems;
    }

    @Override
    public int getCount() {
        return inboxItems.size();
    }

    @Override
    public Object getItem(int position) {
        return inboxItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.inbox_item, null);
            //convertView = feedItemLayout;
        }

        item = inboxItems.get(position);
        VolleySingleton.setApplication(applicationContext);
        imageLoader = VolleySingleton.getInstance().getImageLoader();

        NetworkImageView pictureToUse = (NetworkImageView) convertView.findViewById(R.id.inboxUserPicture);
        TextView subject = (TextView) convertView.findViewById(R.id.inboxSubject);
        TextView last = (TextView) convertView.findViewById(R.id.inboxLastMessage);
        TextView date = (TextView) convertView.findViewById(R.id.inboxDate);

        pictureToUse.setImageUrl(item.inboxHash.get("picture"),imageLoader);
        subject.setText(item.inboxHash.get("subject"));
        last.setText(item.inboxHash.get("last_username") + ": " + item.inboxHash.get("last_message"));
        date.setText(item.inboxHash.get("created_at"));
        Log.d("just",last.getText().toString());

        return convertView;
    }
}
