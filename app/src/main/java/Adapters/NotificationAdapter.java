package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.felix.goingvis_prototype.R;

import java.util.List;

import Classes.VolleySingleton;

/**
 * Created by felix on 1/25/17.
 */

public class NotificationAdapter extends BaseAdapter {
    List<Classes.Notification> myList;
    ImageLoader mImageLoader;
    View feedItemLayout;
    Application applicationContext;
    Activity activity;
    private LayoutInflater inflater;
    Classes.Notification item;


    public NotificationAdapter(Activity activity, List<Classes.Notification> feedItems)
    {
        this.activity = activity;
        this.myList = feedItems;
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.notification_item, null);
        }

        item = myList.get(position);
        VolleySingleton.setApplication(applicationContext);
        mImageLoader = VolleySingleton.getInstance().getImageLoader();

        NetworkImageView picture = (NetworkImageView) convertView.findViewById(R.id.notificationPicture);
        TextView name = (TextView) convertView.findViewById(R.id.notificationName);
        TextView timestamp = (TextView) convertView.findViewById(R.id.notificationTimestamp);
        TextView details = (TextView) convertView.findViewById(R.id.notificationDetails);
        ImageView seen = (ImageView) convertView.findViewById(R.id.seen);

        picture.setImageUrl(item.picture(),mImageLoader);
        name.setText(item.name());
        timestamp.setText(item.timestamp());
        details.setText(item.details());

        if(!item.seen()) {
            seen.setVisibility(View.VISIBLE);
        }else{
            seen.setVisibility(View.GONE);
        }

        return convertView;
    }
}
