package Adapters;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.felix.goingvis_prototype.R;

import java.util.List;

import Classes.ChatItem;
import Classes.VolleySingleton;
import model.UserData;
import UtilityFunctions.CircularNetworkImageView;

/**
 * Created by felix on 12/11/16.
 */

public class ConversationAdapter extends BaseAdapter {
    UserData user;
    LayoutInflater inflater;
    Application applicationContext;
    Activity activity;
    private List<ChatItem> chatItems;
    ChatItem item;
    ImageLoader imageLoader;

    public ConversationAdapter(Activity activity, List<ChatItem> chatItems, Application application){
        this.activity = activity;
        applicationContext = application;
        this.chatItems = chatItems;
    }

    @Override
    public int getCount() {
        return chatItems.size();
    }

    @Override
    public Object getItem(int position) {
        return chatItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.chat_item, null);
            //convertView = feedItemLayout;
        }

        item = chatItems.get(position);
        VolleySingleton.setApplication(applicationContext);
        imageLoader = VolleySingleton.getInstance().getImageLoader();

        CircularNetworkImageView userPic = (CircularNetworkImageView) convertView.findViewById(R.id.messagePicture);
        TextView fullName = (TextView) convertView.findViewById(R.id.messageName);
        TextView message = (TextView) convertView.findViewById(R.id.messageBody);
        TextView timestamp = (TextView) convertView.findViewById(R.id.messageTimestamp);

        userPic.setImageUrl(item.picture,imageLoader);
        fullName.setText(item.name);
        message.setText(item.message);
        timestamp.setText(item.date);

        return convertView;
    }
}
