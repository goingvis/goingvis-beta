package Helpers;

import android.util.Log;

import java.util.concurrent.ExecutionException;

import RestActions.ActivateUser;
import RestActions.CreateMobileVerification;
import RestActions.GetUser;
import RestActions.GetUserFeed;
import RestActions.NewUser;
import RestActions.ProductCatalog;
import RestActions.Sessions;

/**
 * Created by felix on 8/8/16.
 * Collection of Function pertaining to Rest Action
 */
public class Rest {
    public String data = null;
    public String userData = null;
    public String userJson = null;


    /* Getting all products */
   public String productCatalog(){
        //ProductCatalog p = new ProductCatalog();
        //p.execute();
        return null;
    }

    /* Setting up login attempt */
    public Boolean login(String email,String password) throws ExecutionException, InterruptedException {
        Sessions l = new Sessions();
        l.execute(email,password).get();
        if (l.data.equals("Error")){
            return false;
        }
        else{
            data = l.data;
            return true;
        }
    }

    /* Get user */
    public String get_user_json(String usernameOrEmail) throws ExecutionException, InterruptedException {
        GetUser user = new GetUser();
        user.execute(usernameOrEmail).get();
        userJson = user.data;
        return user.data;
    }

    /* Signing up for account */
    public Boolean signup(String username,String full_name,String email, String password) throws ExecutionException, InterruptedException {
        NewUser user = new NewUser();
        Log.d("Data","username: " + username + " FULL NAME: " + full_name + " Email: " + email + " Password: " +  password);
        user.execute(username,full_name,email,password).get();
        if(user.data.equals("Error")){
            return false;
        }
        else{
            userData = get_user_json(username);
            data = user.data;
            return true;
        }
    }


    /* Creating A Verification Code */
    public Boolean createMobileVerification(String number,String username) throws ExecutionException, InterruptedException {
        CreateMobileVerification verify = new CreateMobileVerification();
        verify.execute(number,username).get();
        if(verify.data.equals("Error")){
            return false;
        }
        else{
            return true;
        }
    }

    /* Creating A Verification Code */
    public Boolean verify_code(String number,String username) throws ExecutionException, InterruptedException {
        ActivateUser verify = new ActivateUser();
        verify.execute(number,username).get();
        if(verify.data.equals("Error")){
            return false;
        }
        else{
            return true;
        }
    }


    public Boolean get_users_feed(String username,String page, boolean sync) throws ExecutionException, InterruptedException {
        GetUserFeed feed = new GetUserFeed();
        if(sync) {
            feed.execute(username, page);
        }
        else {
            feed.execute(username, page).get();
        }

        if(feed.data.equals("Error")){
            return false;
        }
        else{
            data = feed.data;
            return true;
        }
    }


}
