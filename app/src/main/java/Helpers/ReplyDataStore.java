package Helpers;

import android.view.View;

import java.util.HashMap;

import Adapters.ReplyAdapter;
import model.ReplyData;

/**
 * Created by felix on 11/4/16.
 */

public class ReplyDataStore {
    HashMap<String,ReplyAdapter> replyAdapters = new HashMap<>();
    HashMap<String,ReplyData> replyData = new HashMap<>();
    HashMap<String,View> replyView = new HashMap<>();

    public ReplyAdapter get_reply_adapter(String id)
    {
        return replyAdapters.get(id);
    }

    public ReplyData get_reply_data(String id)
    {
        return replyData.get(id);
    }
    public View get_reply_view(String id){return replyView.get(id);}

    public void add(ReplyAdapter ra,ReplyData rd, String id, View v)
    {
        replyAdapters.put(id,ra);
        replyData.put(id,rd);
        replyView.put(id,v);
    }
}
