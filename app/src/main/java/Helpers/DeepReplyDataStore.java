package Helpers;

import android.view.View;

import java.util.HashMap;

import Adapters.DeepReplyAdapter;
import model.DeepReplyData;

/**
 * Created by felix on 11/19/16.
 */

public class DeepReplyDataStore {
    HashMap<String,DeepReplyAdapter> deepReplyAdapters = new HashMap<>();
    HashMap<String, DeepReplyData> deepReplyData = new HashMap<>();
    HashMap<String,View> deepReplyView = new HashMap<>();

    public DeepReplyAdapter get_reply_adapter(String id)
    {
        return deepReplyAdapters.get(id);
    }

    public DeepReplyData get_reply_data(String id)
    {
        return deepReplyData.get(id);
    }
    public View get_reply_view(String id){return deepReplyView.get(id);}

    public void add(DeepReplyAdapter ra, DeepReplyData rd, String id, View v)
    {
        deepReplyAdapters.put(id,ra);
        deepReplyData.put(id,rd);
        deepReplyView.put(id,v);
    }
}