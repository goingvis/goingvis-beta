package UtilityFunctions;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by felix on 8/12/16.
 */
public class SaveSharedPreference
{
    private static final String PREF_USER_NAME= "username";
    private static final String PREF_USER_EMAIL= "useremail";
    private static final String PREF_USER_PASSWORD= "password";
    private static final String PREF_USER_RememberMeStatus= "RememberMeStatus";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void setUserEmail(Context ctx, String useremail)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_EMAIL, useremail);
        editor.commit();
    }

    public static String getUserEmail(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_EMAIL, "");
    }

    public static void setUserPassword(Context ctx, String userpassword)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_PASSWORD, userpassword);
        editor.commit();
    }

    public static String getUserPassword(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_PASSWORD, "");
    }

    public static void setRememberMeStatus(Context ctx, Boolean RememberMeStatus)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putBoolean(PREF_USER_RememberMeStatus, RememberMeStatus);
        editor.commit();
    }

    public static Boolean getRememberMeStatus(Context ctx)
    {
        return getSharedPreferences(ctx).getBoolean(PREF_USER_RememberMeStatus, true);
    }
}
