package RestActions;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import Classes.Utility;

/**
 * Created by felix on 8/14/16.
 */
public class GetUserFeed extends AsyncTask {
    public String data;
    public JSONObject json;

    public GetUserFeed(){}

    public interface AsyncResponse {
        void processFinish(String output) throws JSONException, ExecutionException, InterruptedException;

    }

    public AsyncResponse delegate = null;

    public GetUserFeed(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected void onPostExecute(Object result) {
        //data = (String) result;
        if(delegate != null)
        {
            try {
                delegate.processFinish(data);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        Log.d("CMON","Get user profile via REST: " + params[0]);
        // Will contain the raw JSON response as a string.
        String jsonStr = null;
        try
        {
            String u;
            // Determine if grabbing entire feed or just a single micropost
            if(params[2].toString().length() > 0)
            {
                 u = Utility.cleanUrl("https://www.goingvis.com/api/v1/get_micropost?id=" + params[0] +  Utility.apiUrl);
            }
            else
            {
                 u = Utility.cleanUrl("https://www.goingvis.com/api/v1/get_user_news_feed?username=" + params[0] + "&feed_page=" + params[1] + Utility.apiUrl);
            }
            URL url = new URL(u);

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null)
            {
                // Nothing to do.
                //forecastJsonStr = null;
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null)
            {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");

            }

            if (buffer.length() == 0)
            {
                // Stream was empty.  No point in parsing.
                //forecastJsonStr = null;
                return null;
            }
            jsonStr = buffer.toString();
            Log.d("JSON COMMAND: ", jsonStr);
            data = jsonStr;
            //weather.add(forecastJsonStr);
        } catch (MalformedURLException | ProtocolException e)
        {
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Test", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
            jsonStr = null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("Test", "Error closing stream", e);
                }
            }
        }
        return null;
    }

}
