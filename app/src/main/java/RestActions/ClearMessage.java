package RestActions;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import Classes.Utility;

/**
 * Created by felix on 1/27/17.
 */

public class ClearMessage extends AsyncTask {
    public String data;
    public JSONObject json;

    public ClearMessage(){}

    public interface AsyncResponse {
        void processFinish(String output) throws JSONException;

    }

    public AsyncResponse delegate = null;

    public ClearMessage(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected void onPostExecute(Object result) {
        if(delegate != null)
        {
            try {
                delegate.processFinish(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        // Will contain the raw JSON response as a string.
        String jsonStr = null;
        try
        {

            String u = Utility.cleanUrl("https://www.goingvis.com/api/v1/clear_message_notification?id=" + params[0] + Utility.apiUrl);
            URL url = new URL(u);

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) { return null; }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) { buffer.append(line + "\n"); }

            if (buffer.length() == 0)
            {
                // Stream was empty.  No point in parsing.
                //forecastJsonStr = null;
                return null;
            }
            jsonStr = buffer.toString();
            data = jsonStr;
            //weather.add(forecastJsonStr);
        } catch (MalformedURLException | ProtocolException e)
        {
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Test", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
            jsonStr = null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("Test", "Error closing stream", e);
                }
            }
        }
        return null;
    }

}


