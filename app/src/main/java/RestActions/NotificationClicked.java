package RestActions;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import Classes.Utility;

/**
 * Created by felix on 1/27/17.
 */

public class NotificationClicked extends AsyncTask
{
    public String d;
    public String data;

    public NotificationClicked(){}
    public interface AsyncResponse {
        void processFinish(String output) throws JSONException;

    }

    public AsyncResponse delegate = null;

    public NotificationClicked(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected void onPostExecute(Object result)
    {
        //data = (String) result;
        if(delegate != null)
        {
            try
            {
                delegate.processFinish(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected Object doInBackground(Object[] params) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String forecastJsonStr = null;
        String SanJose = "San%20Jose,US";
        URL url;
        try
        {

            url = new URL("https://www.goingvis.com/api/v1/notification_click?id=" + params[0]  + Utility.apiUrl);
            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null)
            {
                // Nothing to do.
                //forecastJsonStr = null;
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null)
            {
                buffer.append(line + "\n");
            }
            if (buffer.length() == 0)
            {
                return null;
            }
            forecastJsonStr = buffer.toString();
            data = forecastJsonStr;
            Log.d("JSON COMMAND: ", forecastJsonStr);
            //weather.add(forecastJsonStr);
        } catch (MalformedURLException | ProtocolException e)
        {
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Test", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
            forecastJsonStr = null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("Test", "Error closing stream", e);
                }
            }
        }
        return null;
    }

}



