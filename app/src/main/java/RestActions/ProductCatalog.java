package RestActions;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import Classes.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by felix on 8/8/16.
 * Rest action to load all the products
 */
public class ProductCatalog extends AsyncTask
{
    public String d;
    public String data;
    public interface AsyncResponse {
        void processFinish(String output) throws JSONException;

    }

    public AsyncResponse delegate = null;

    public ProductCatalog(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected void onPostExecute(Object result)
    {
        //data = (String) result;
        if(delegate != null)
        {
            try
            {
                delegate.processFinish(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected Object doInBackground(Object[] params) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String forecastJsonStr = null;
        String SanJose = "San%20Jose,US";
        URL url;
        try
        {
            if(params.length > 0)
            {
                if(params.length == 1) {
                    url = new URL("https://www.goingvis.com/api/v1/search?search=" + URLEncoder.encode((String) params[0], "UTF-8") + Utility.apiUrl);
                }else {
                    url = new URL("https://www.goingvis.com/api/v1/search?search=" + URLEncoder.encode((String) params[0], "UTF-8") + "&page=" + params[1] + Utility.apiUrl);
                }
            }
            else
            {
                url = new URL("https://www.goingvis.com/api/v1/products" + Utility.apiUrl);
            }

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null)
            {
                // Nothing to do.
                //forecastJsonStr = null;
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null)
            {
                buffer.append(line + "\n");
            }
            if (buffer.length() == 0)
            {
                return null;
            }
            forecastJsonStr = buffer.toString();
            data = forecastJsonStr;
            Log.d("JSON COMMAND: ", forecastJsonStr);
            //weather.add(forecastJsonStr);
        } catch (MalformedURLException | ProtocolException e)
        {
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Test", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
            forecastJsonStr = null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("Test", "Error closing stream", e);
                }
            }
        }
        return null;
    }

}



