package com.example.felix.goingvis_prototype;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONException;

import Adapters.NotificationAdapter;
import Classes.Notification;
import Classes.Utility;
import model.NotificationData;
import model.UserData;
import RestActions.GetNotifications;
import RestActions.NotificationClicked;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Notifications.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Notifications#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Notifications extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private UserData user;
    NotificationData notificationData;
    ListView listView;
    NotificationAdapter listAdapter;
    int page = 1;

    private OnFragmentInteractionListener mListener;

    View rootView;

    public Notifications() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Notifications.
     */
    // TODO: Rename and change types and number of parameters
    public static Notifications newInstance(String param1, String param2) {
        Notifications fragment = new Notifications();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_notifications, container, false);

        user = (UserData) getActivity().getApplicationContext();
        notificationData = new NotificationData();
        final View layout = inflater.inflate(R.layout.notification_item,container);
        listView = (ListView) rootView.findViewById(R.id.notificationList);
        //viewPager = (ViewPager) rootView.findViewById(R.id.pager);

        final ProgressBar spinner = (ProgressBar) rootView.findViewById(R.id.loadNotification);
        spinner.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);

        //spinner.getIndeterminateDrawable().setColorFilter(0x009900, android.graphics.PorterDuff.Mode.MULTIPLY);
        //spinner.setVisibility(View.VISIBLE);

        //layout.setVisibility(View.GONE);

        new GetNotifications(new GetNotifications.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                notificationData.setNotificationList(output);
                listAdapter = new NotificationAdapter(getActivity(),notificationData.notificationList);
                listView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();
                listView.setOnItemClickListener( listener() );
                listView.setItemsCanFocus(true);

                listView.setOnScrollListener(new AbsListView.OnScrollListener() {

                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                                && (listView.getLastVisiblePosition() - listView.getHeaderViewsCount() -
                                listView.getFooterViewsCount()) >= (listAdapter.getCount() - 1)) {
                            page++;
                            new GetNotifications(new GetNotifications.AsyncResponse() {

                                @Override
                                public void processFinish(String output) throws JSONException {
                                    notificationData.setNotificationList(output);
                                    listAdapter.notifyDataSetChanged();
                                }
                            }).execute(user.id(),mParam1,page);
                        }
                    }
                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {}
                });

                spinner.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }
        }).execute(user.id(),mParam1,page);

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public AdapterView.OnItemClickListener listener() {
        try {
            AdapterView.OnItemClickListener listt =  new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    Notification item = (Notification) listAdapter.getItem(position);
                    Intent openFrag = new Intent(getActivity(), MainActivity.class);
                    String [] arr = new String [] {Utility.notificationType(item.hashtable),item.hashtable.get("id_to_link")};
                    openFrag.putExtra("fragment",arr);
                    if(item.hashtable.get("id_to_link")!= null && item.hashtable.get("id_to_link").length() > 0)
                    {
                        new NotificationClicked().execute(item.id());
                        //new ClearNotifications().execute(user.id());
                        new MyReceiver().onReceive(getActivity(), openFrag);
                    }
                }

            };
            return listt;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
