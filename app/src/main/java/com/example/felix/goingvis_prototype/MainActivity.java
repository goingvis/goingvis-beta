package com.example.felix.goingvis_prototype;

import android.app.Activity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ExecutionException;

import Classes.Utility;
import Dialogs.NewPostFragment;
import Dialogs.NotificationPopup;
import Helpers.Rest;
import RestActions.ClearInteraction;
import RestActions.ClearMessage;
import RestActions.ClearNotificationToken;
import RestActions.GetFriendsUsername;
import RestActions.GetNotificationCount;
import RestActions.GetUser;
import model.User;
import model.UserData;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,NewsFeed.OnFragmentInteractionListener,
                   LandingPage.OnFragmentInteractionListener, Settings.OnFragmentInteractionListener,
                   SearchView.OnQueryTextListener,SearchView.OnCloseListener, ProductList.OnFragmentInteractionListener,
                   Product.OnFragmentInteractionListener, Inbox.OnFragmentInteractionListener,
                   Conversation.OnFragmentInteractionListener,
                   Notifications.OnFragmentInteractionListener, ShowMicropost.OnFragmentInteractionListener{

    private String json_data;
    Hashtable<String, String> userTable;
    NewsFeed ns;
    Fragment mainFragment;
    Fragment activeFragment;
    FloatingActionButton new_post_button;
    UserData user;
    SearchView searchView;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    boolean dbListnerSet = false;
    Activity myActivity;

    // Set notification icons
    View messageNotification = null;
    View importantNotification = null;
    View interactionNotification = null;

    TextView messageCount;
    TextView importantCount;
    TextView interactionCount;
    UserData me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        user = (UserData) getApplicationContext();
        String notificationToken = FirebaseInstanceId.getInstance().getToken();

        if (Utility.already_logged_in(this)) {
            Rest r = new Rest();
            try {
                r.get_user_json(Utility.username);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                boolean s = r.userJson.equalsIgnoreCase("null\0");
                int count = r.userJson.length();
                String str = String.valueOf(s);

                if (r.userJson.length() == 5) {
                    Utility.logout(this);
                    Intent i = new Intent(this, Login.class);
                    startActivity(i);
                }
                user.setUser(r.userJson,notificationToken);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Intent i = new Intent(this, Login.class);
            startActivity(i);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        EditText tx = (EditText)toolbar.findViewById(R.id.edt_search);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/BoosterNextFY-Light.otf");
        tx.setTypeface(custom_font);

        setSupportActionBar(toolbar);
        create_notify_bubbles();

        me = (UserData) getApplicationContext();

        new GetFriendsUsername(new GetFriendsUsername.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                me.setFriends(output);
            }
        }).execute(me.username());


        // Creating5
        new_post_button = (FloatingActionButton) findViewById(R.id.new_post_button);
        new_post_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewPostFragment editNameDialog = new NewPostFragment();
                editNameDialog.show(getSupportFragmentManager(), "fragment_edit_name");
            }
        });

//        new_post_button.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_drawer);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
//        toggle.setHomeAsUpIndicator(R.drawable.ic_drawer); //set your own
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        // Getting the API Data
        //json_data = get_products();
        TextView headerView = (TextView) LayoutInflater.from(this).inflate(R.layout.products, null);
        headerView.setText(json_data);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Added the Header View Which contains the JSON data
        navigationView.addHeaderView(headerView);

        navigationView.setNavigationItemSelectedListener(this);
        myActivity = this;

        // Load Fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Intent myActivity = getIntent();
        final String[] fragArray = myActivity.getStringArrayExtra("fragment");
        if (fragArray != null) {
            switch (fragArray[0])
            {
                case "newsfeed":
                    Log.d("About to try and load", fragArray[1]);
                    new GetUser(new GetUser.AsyncResponse() {
                        @Override
                        public void processFinish(String output) throws JSONException {
                            activeFragment =  NewsFeed.newInstance(fragArray[1],fragArray[1],new User(output),"");
//                            activeFragment = new NewsFeedTabbed().newInstance(fragArray[1], fragArray[1], new User(output));
                            //activeFragment = new NewsFeedTabbed().newInstance(fragArray[1],fragArray[1]);
                            fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();
                        }
                    }).execute(fragArray[1]);
                    break;
                case "product":
                    Log.d("About to load product", fragArray[1]);
                    activeFragment = new Product().newInstance(fragArray[1], fragArray[1]);
                    fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();
                    break;
                case "conversation":
                    Log.d("Will Send",fragArray[1]);
                    new ClearMessage().execute(user.id());
                    activeFragment = new Conversation().newInstance(fragArray[1], fragArray[1]);
                    fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();
                    break;
                case "micropost":
                    new ClearInteraction().execute(user.id());
                    activeFragment = new NewsFeed().newInstance(fragArray[1],fragArray[1],null,fragArray[1]);
                    fragmentTransaction.add(R.id.fragment_container,activeFragment).commit();
                    break;
            }
        } else {
            activeFragment =  NewsFeed.newInstance(user.username(),Utility.username,null,"");
//            activeFragment =  new NewsFeedTabbed().newInstance(user.username(),Utility.username,null);
            fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();
        }
        /****** Creating Rest Section ****/
        //Rest rest = new Rest();
        //json_data = rest.productCatalog();
        set_database_listener();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);


        MenuItem item = menu.findItem(R.id.badge);
        //MenuItemCompat.setActionView(item, R.layout.notifications_icon);
        RelativeLayout notifview = (RelativeLayout) MenuItemCompat.getActionView(item);
        notifview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationPopup editNameDialog = new NotificationPopup();
                editNameDialog.show(getSupportFragmentManager(),"");
            }
        });

        // Get views for important notification
        importantCount = (TextView) notifview.findViewById(R.id.importantCount);
        importantNotification = (View) notifview.findViewById(R.id.importantIcon);

        //Get views for message notifications
        messageCount = (TextView) notifview.findViewById(R.id.messageCount);
        messageNotification = (View) notifview.findViewById(R.id.messageIcon);

        //Get views for interaction notifications
        interactionCount = (TextView) notifview.findViewById(R.id.interactionCount);
        interactionNotification = (View) notifview.findViewById(R.id.interactionIcon);

        //messageCount = (Text)



        //restoreActionBar();
        // Associate searchable configuration with the SearchView
        //SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        //searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        setupSearchView();

        create_notify_bubbles();

        return super.onCreateOptionsMenu(menu);
    }

    public void create_notify_bubbles()
    {
        new GetNotificationCount(new GetNotificationCount.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                Hashtable<String,String> myMap =  Utility.singleRestOutputToHash(output);
                Log.d("Inbox JSON",myMap.get("inbox") + myMap.get("interaction"));
                if(myMap.get("inbox").equalsIgnoreCase("0") || myMap.get("inbox").toString().contains("null")){
                    if(messageNotification != null) {
                        messageNotification.setVisibility(View.GONE);
                    }
                }else{
                    if(messageNotification != null) {
                        messageNotification.setVisibility(View.VISIBLE);
                    }
                    if( messageCount != null ){
                        messageCount.setText(myMap.get("inbox"));
                    }
                }
                // Interaction bubble
                if(myMap.get("interaction").equalsIgnoreCase("0") || myMap.get("interaction").toString().contains("null")){
                    if(interactionNotification != null)
                    {
                        interactionNotification.setVisibility(View.GONE);
                    }
                }else{
                    if(interactionNotification != null){
                        interactionNotification.setVisibility(View.VISIBLE);
                    }
                    if( interactionCount != null ) {
                        interactionCount.setText(myMap.get("interaction"));
                    }
                }
                // Important bubble
                if(myMap.get("important").equalsIgnoreCase("0") || myMap.get("important").toString().contains("null")){
                    if(importantNotification != null){
                        importantNotification.setVisibility(View.GONE);
                    }
                }else{
                    if(importantNotification != null){
                        importantNotification.setVisibility(View.VISIBLE);
                    }
                    if( importantCount != null ){
                        importantCount.setText(myMap.get("important"));
                    }
                }
            }
        }).execute(user.id());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().remove(activeFragment);
//        new_post_button.setVisibility(View.GONE);
        //ns = new NewsFeed();

        if (id == R.id.news_feed) {
//            activeFragment =  new NewsFeedTabbed().newInstance(user.username(),Utility.username,null);
            activeFragment =  NewsFeed.newInstance(user.username(),Utility.username,null,"");
            fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();
        } else if (id == R.id.settings) {
            activeFragment = new Settings().newInstance("username", Utility.username);
            fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();

        } else if (id == R.id.inbox) {
            activeFragment = new Inbox().newInstance("username", Utility.username);
            fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();

        } else if (id == R.id.logout) {
            new ClearNotificationToken().execute(user.username());
            Utility.logout(this);
            Intent i = new Intent(this, Login.class);
            startActivity(i);
            finish();
        } else {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupSearchView() {

//        searchView.setIconifiedByDefault(true);
//        searchView.setFocusable(true);
//        searchView.setIconified(false);
//        searchView.requestFocusFromTouch();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();

            // Try to use the "applications" global search provider
            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
            for (SearchableInfo inf : searchables) {
                if (inf.getSuggestAuthority() != null && inf.getSuggestAuthority().startsWith("applications")) {
                    info = inf;
                }
            }
            searchView.setSearchableInfo(info);
        }

        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onClose() {
        Log.d("Searc", "'close");
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("Searc", query);

        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().remove(activeFragment);
        activeFragment = new ProductList().newInstance("search", query,"");
        fragmentTransaction.add(R.id.fragment_container, activeFragment).commit();

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d("Searc", newText);
        return false;
    }

    // Set on database listener and clear if notification is present...
    public void set_database_listener()
    {
        if(!dbListnerSet) {
            database.child(user.id()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String text = dataSnapshot.getValue().toString();
                    if (!text.equalsIgnoreCase("null")) {
                        JSONObject data = new JSONObject((HashMap) dataSnapshot.getValue());
                        try {
                            Hashtable<String,String> hash = Utility.jsonToHash(data);
                            if(hash.get("type").equalsIgnoreCase("notification_bubble")){
                                Log.d("Notify Bubble","Yes");
                                if(hash.get("class").equalsIgnoreCase("message_bubble"))
                                {
                                    if( user.getCurrentConversation() == null || !user.getCurrentConversation().equalsIgnoreCase(hash.get("id"))) {
                                        create_notify_bubbles();
                                    }
                                }
                                else if(hash.get("class").equalsIgnoreCase("clear"))
                                {
                                    create_notify_bubbles();
                                }
                                else
                                {
                                    create_notify_bubbles();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            dbListnerSet = true;
        }
    }
}
