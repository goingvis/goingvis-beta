
package com.example.felix.goingvis_prototype;


import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;

import Classes.Utility;


public class SplashActivity extends Activity {

    protected int _splashTime = 1500;
    private Thread splashTread;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized(this){
                        wait(_splashTime);
                    }

                } catch(InterruptedException e) {}
                finally {
                    CheckIfIUserLoggedin();
                    finish();
                }
                    try {
                        sleep(_splashTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

        };

        splashTread.start();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            synchronized(splashTread){
                splashTread.notifyAll();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            splashTread.interrupt();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    private void CheckIfIUserLoggedin() {

        if(!Utility.already_logged_in(this)){
            Intent intent = new Intent(this,Login.class);
            startActivity(intent);
        } else {
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
        }
    }
}
