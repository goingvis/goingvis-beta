package com.example.felix.goingvis_prototype;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import Adapters.GalleryAdapter;
import Classes.Utility;
import Classes.Variation;
import Classes.VolleySingleton;
import model.UserData;
import RestActions.GetProduct;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Product.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Product#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Product extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Classes.Product product = new Classes.Product();
    ViewPager viewPager;
    GalleryAdapter galleryAdapter;
    ArrayList<String> quantities;
    ArrayList<Variation> variations = new ArrayList<>();
    ArrayAdapter<String> quantityAdapter;
    ArrayAdapter<String> optionAdapter;
    ArrayList<String> options = new ArrayList<>();
    // Views, Text Fields and more...
    View rootView;
    TextView title;
    TextView price;
    TextView condition;
    TextView  ship;
    Spinner optionSpinner;
    TextView numberReviews,productDetails;
    // Three buttons variables
    TextView sellersNote;
    Button sellersNoteButton;
    Button shippingPolicyButton;
    Button aboutSellerButton;
    View sellersNoteLayout;
    View shippingPolicyLayout;
    View aboutSellerLayout;
    NetworkImageView productSellerPicture;
    ImageLoader imageLoader;
    TextView productShipFrom,productHandleTime,productReturn,productRefund,productSellerName;
    UserData user;

    private OnFragmentInteractionListener mListener;

    public Product() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Product.
     */
    // TODO: Rename and change types and number of parameters
    public static Product newInstance(String param1, String param2) {
        Product fragment = new Product();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_product, container, false);
        user = (UserData) getActivity().getApplicationContext();
        VolleySingleton.setApplication(user);
        imageLoader = VolleySingleton.getInstance().getImageLoader();

        title = (TextView) rootView.findViewById(R.id.productTitle);
        price = (TextView) rootView.findViewById(R.id.productPrice);
        condition = (TextView) rootView.findViewById(R.id.productCondition);
        ship = (TextView) rootView.findViewById(R.id.productShip);
        optionSpinner = (Spinner) rootView.findViewById(R.id.productVariationOption);
        numberReviews = (TextView) rootView.findViewById(R.id.productNumberReviews);
        productDetails = (TextView) rootView.findViewById(R.id.productDetails);
        // Three buttons variables
        sellersNote = (TextView) rootView.findViewById(R.id.sellersNote);
        sellersNoteButton = (Button) rootView.findViewById(R.id.sellersNoteButton);
        shippingPolicyButton = (Button) rootView.findViewById(R.id.shippingPolicyButton);
        aboutSellerButton = (Button) rootView.findViewById(R.id.aboutSellerButton);
        sellersNoteLayout =  rootView.findViewById(R.id.sellersNoteLayout);
        shippingPolicyLayout =  rootView.findViewById(R.id.shippingPolicyLayout);
        aboutSellerLayout = rootView.findViewById(R.id.aboutSellerLayout);

        productShipFrom = (TextView) rootView.findViewById(R.id.productShipFrom);
        productHandleTime = (TextView) rootView.findViewById(R.id.productHandleTime);
        productRefund = (TextView) rootView.findViewById(R.id.productRefund);
        productReturn = (TextView) rootView.findViewById(R.id.productReturn);

        productSellerName = (TextView) rootView.findViewById(R.id.productSellerName);
        productSellerPicture = (NetworkImageView) rootView.findViewById(R.id.productSellerPicture);

        setButtonActions();

        // Setting Product
        try {
            product.set_product(mParam1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Recreate product
        new GetProduct(new GetProduct.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                product.set_product(output);
                // Set Pictures
                viewPager = (ViewPager) rootView.findViewById(R.id.productPager);
                try {
                    galleryAdapter = new GalleryAdapter(getActivity(),product.galleryImages(),getActivity().getApplication());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                viewPager.setAdapter(galleryAdapter);
                galleryAdapter.notifyDataSetChanged();
                // Set Basic Titles
                title.setText(product.productHash.get("name"));
                price.setText(product.productHash.get("price"));
                condition.setText(product.productHash.get("condition"));
                ship.setText(product.productHash.get("shipping_info"));
                numberReviews.setText(product.productHash.get("review_count") + " reviews");
                sellersNote.setText(product.productHash.get("sellers_note"));

                productHandleTime.setText(Html.fromHtml("<strong> Handle Time </strong><br>" + product.productHash.get("handle_time")));
                productReturn.setText(Html.fromHtml("<strong>Return Within </strong><br>" + product.productHash.get("returnType")));
                productRefund.setText(Html.fromHtml("<strong>Refund As </strong><br>" + product.productHash.get("refundAs")));
                productShipFrom.setText(Html.fromHtml("<strong> Ship From </strong><br>" + product.productHash.get("ship_from")));

                productSellerName.setText(product.productHash.get("sellers_username"));
                productSellerPicture.setImageUrl(product.productHash.get("sellers_picture"),imageLoader);
                productDetails.setText(Html.fromHtml(product.productHash.get("detailedDesc")));

                // Start Dropdown logics
                quantities = new ArrayList<>();
                Spinner dropdown = (Spinner)rootView.findViewById(R.id.productQuantity);
                int initQuantity;
                if(product.productHash.get("variation").toString().equalsIgnoreCase("no"))
                {
                    initQuantity = Integer.parseInt(product.productHash.get("quantity"));
                    optionSpinner.setVisibility(View.GONE);
                }
                else
                {
                    optionSpinner.setVisibility(View.VISIBLE);
                    // Set Variation
                    final String var = product.productHash.get("variation").toString();
                    JSONArray json = new JSONArray(var);
                    for (int i = 0; i < json.length(); i++)
                    {
                        //JSONObject j = json.optJSONObject(Integer.parseInt(String.valueOf(i)));
                        Log.d("Set Micropost at i: ", Integer.toString(i));
                        Variation m = new Variation();
                        m.set_variation(json.getJSONObject(i).toString());
                        variations.add(m);
                        options.add(m.option);
                    }
                    initQuantity = variations.get(0).quantity;
                    variations.get(0).active = true;
                    price.setText("$" + variations.get(0).price);
                    optionAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, options);
                    optionSpinner.setAdapter(optionAdapter);
                    optionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            quantities.clear();
                            for(int i = 0; i < variations.get(position).quantity; i++)
                                {quantities.add(String.valueOf(i+1));}
                            price.setText("$" + variations.get(position).price);
                            variations.get(position).active = true;
                            for(int i = 0; i < variations.size(); i++)
                                {if(i != position) {variations.get(i).active = false;}}
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }

                for(int i = 0; i < initQuantity; i++)
                {
                    quantities.add(String.valueOf(i+1));
                }

                quantityAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, quantities);
                dropdown.setAdapter(quantityAdapter);

                // Setting Rating
                ArrayList<String> stars = Utility.setStarsProduct(product);
                Utility.setBackgroundStar(stars,rootView);

                shippingPolicyLayout.setVisibility(View.GONE);
                aboutSellerLayout.setVisibility(View.GONE);

            }
        }).execute(product.productHash.get("url_id"));

        Log.d("Set Product in Frag",product.productHash.toString());
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    void setButtonActions()
    {
        sellersNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("View Click","Sellers Note");
                sellersNoteLayout.setVisibility(View.VISIBLE);
                shippingPolicyLayout.setVisibility(View.GONE);
                aboutSellerLayout.setVisibility(View.GONE);
            }
        });

        shippingPolicyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("View Click","Policy");
                sellersNoteLayout.setVisibility(View.GONE);
                shippingPolicyLayout.setVisibility(View.VISIBLE);
                aboutSellerLayout.setVisibility(View.GONE);
            }
        });

        aboutSellerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("View Click","About Seller");
                sellersNoteLayout.setVisibility(View.GONE);
                shippingPolicyLayout.setVisibility(View.GONE);
                aboutSellerLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
