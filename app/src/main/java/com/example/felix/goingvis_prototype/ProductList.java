package com.example.felix.goingvis_prototype;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;

import java.util.List;

import Adapters.ProductAdapter;
import Classes.Product;
import Classes.Utility;
import model.ProductData;
import model.UserData;
import RestActions.GetUserProducts;
import RestActions.ProductCatalog;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductList extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private View rootView;
    ListView listView;
    UserData user;
    ProductData productData = new ProductData();
    List<Product> products;
    ProductAdapter listAdapter;
    private int page_number = 1;
    ProgressBar spinner;
    TextView noProductsText;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String username;

    private OnFragmentInteractionListener mListener;

    public ProductList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductList.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductList newInstance(String param1, String param2, String param3) {
        ProductList fragment = new ProductList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3,param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            username = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_product_list, container, false);
        user = (UserData) getActivity().getApplicationContext();
        listView = (ListView) rootView.findViewById(R.id.productList);

        spinner = (ProgressBar) rootView.findViewById(R.id.productLoad);
        spinner.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);
        noProductsText = (TextView) rootView.findViewById(R.id.noProductsText);


        if(mParam1.equalsIgnoreCase("search")){
            new ProductCatalog(new ProductCatalog.AsyncResponse() {
                @Override
                public void processFinish(String output) throws JSONException {
                    // Finished Loading the products
                    create_list(output);
                }
            }).execute(mParam2);
        }else{
            new GetUserProducts(new GetUserProducts.AsyncResponse() {
                @Override
                public void processFinish(String output) throws JSONException {
                    create_list(output);
                }
            }).execute(username);
        }


        // Inflate the layout for this fragment
        return rootView;
    }

    void create_list(String output) throws JSONException {
        productData.setProductList(output);
        products = productData.getProductList();
        listAdapter = new ProductAdapter(getActivity(),products);
        listAdapter.setApplicationContext(user);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        listView.setOnItemClickListener( listener() );
        listView.setItemsCanFocus(true);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (listView.getLastVisiblePosition() - listView.getHeaderViewsCount() -
                        listView.getFooterViewsCount()) >= (listAdapter.getCount() - 1)) {
                    page_number++;
                    if(mParam1.equalsIgnoreCase("search")) {
                        new ProductCatalog(new ProductCatalog.AsyncResponse(){

                            @Override
                            public void processFinish(String output) throws JSONException {
                                productData.setProductList(output);
                                listAdapter.notifyDataSetChanged();
                            }
                        }).execute(mParam2,page_number);
                    }else{
                        new GetUserProducts(new GetUserProducts.AsyncResponse() {
                            @Override
                            public void processFinish(String output) throws JSONException {
                                productData.setProductList(output);
                                listAdapter.notifyDataSetChanged();
                            }
                        }).execute(username,page_number);
                    }

                }
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {}
        });

        if(mParam1.equalsIgnoreCase("user")){
            RelativeLayout productListRelative = (RelativeLayout) rootView.findViewById(R.id.productListRelativeLayout);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            Log.d("MarginTop",Double.toString(Utility.dpToPx(Utility.dpiNewsFeedMarginTop,getContext())));
            params.setMargins(0, (int) Utility.dpToPx(Utility.dpiNewsFeedMarginTop,getContext()),0,0);
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            productListRelative.setLayoutParams(params);
        }

        spinner.setVisibility(View.GONE);
        if(products.size() == 0){
            noProductsText.setText("No products listed...");
            noProductsText.setVisibility(View.VISIBLE);
        }else{
            noProductsText.setVisibility(View.GONE);
        }
        listView.setVisibility(View.VISIBLE);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public AdapterView.OnItemClickListener listener() {
        AdapterView.OnItemClickListener listt =  new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                String url = products.get(position).productHash.get("url_id");

                Intent openFrag = new Intent(getActivity(), MainActivity.class);
                String [] arr = new String [] {"product",products.get(position).dataStr};
                openFrag.putExtra("fragment",arr);
                new MyReceiver().onReceive(getActivity(), openFrag);

            }
        };
        return listt;
    }

}
