package com.example.felix.goingvis_prototype;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import Adapters.InboxAdapter;
import Classes.InboxItem;
import Dialogs.NewMessagePopup;
import model.InboxData;
import model.UserData;
import RestActions.GetFriends;
import RestActions.GetInbox;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Inbox.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Inbox#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Inbox extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootView;
    UserData user;
    InboxData inboxData  = new InboxData();
    List<InboxItem> inboxItemList = new ArrayList<>();
    InboxAdapter listAdapter;
    ListView listView;
    Button composeButton;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();


    private OnFragmentInteractionListener mListener;

    public Inbox() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Inbox.
     */
    // TODO: Rename and change types and number of parameters
    public static Inbox newInstance(String param1, String param2) {
        Inbox fragment = new Inbox();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        user = (UserData) getActivity().getApplicationContext();

        // Spinner
        final ProgressBar load  = (ProgressBar) rootView.findViewById(R.id.loadInbox);
        load.setVisibility(View.VISIBLE);

        new GetInbox(new GetInbox.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                Log.d("Just loaded it!",output);
                listView  = (ListView) rootView.findViewById(R.id.inboxList);
                composeButton = (Button) rootView.findViewById(R.id.composeButton);
                inboxData.setInbox(output);
                // Setting microposts
                inboxItemList = inboxData.inboxList;


                listAdapter = new InboxAdapter(getActivity(), inboxItemList, user);

                listView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();
                listView.setOnItemClickListener( listener() );
                listView.setItemsCanFocus(true);

                load.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);

            }
        }).execute(user.username());
        // Inflate the layout for this fragment

        new GetFriends(new GetFriends.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                user.setFriends(output);
                setComposeButton();
            }
        }).execute(user.username());

        //new CreateNotification().execute(user.username(),"New Notification","New message man","message");
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void setComposeButton()
    {
        composeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewMessagePopup editNameDialog = new NewMessagePopup();
                editNameDialog.show(getActivity().getFragmentManager(), "fragment_edit_name");
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public AdapterView.OnItemClickListener listener() {
        AdapterView.OnItemClickListener listt =  new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                InboxItem item = (InboxItem) listAdapter.getItem(position);

                Intent openFrag = new Intent(getActivity(), MainActivity.class);
                String [] arr = new String [] {"conversation",item.inboxHash.get("id")};
                openFrag.putExtra("fragment",arr);
                database.child(user.id()).setValue("null");
                new MyReceiver().onReceive(getActivity(), openFrag);
                Log.d("On Click",item.inboxHash.get("subject"));
            }

        };
        return listt;
    }

}
