package com.example.felix.goingvis_prototype;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import Helpers.Rest;
import model.UserData;

public class VerifyCode extends AppCompatActivity {
    UserData user = null;
    EditText code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        user = (UserData) getApplicationContext();
        code = (EditText) findViewById(R.id.code_text);

    }

    void send_code(View v) throws ExecutionException, InterruptedException {
        String number = code.getText().toString();
        Rest r = new Rest();
        if(r.verify_code(number,user.get_user_hash().get("username").toString())){
            Intent i = new Intent(this,MainActivity.class);
            startActivity(i);
            finish();
        }else{
            Toast.makeText(this,"Invalid Code please try again", Toast.LENGTH_LONG);
        }
    }

}
