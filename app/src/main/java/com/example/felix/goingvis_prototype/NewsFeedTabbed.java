package com.example.felix.goingvis_prototype;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;

import Dialogs.NewPostFragment;
import model.User;
import model.UserData;
import RestActions.GetFriendsUsername;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsFeedTabbed.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewsFeedTabbed#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFeedTabbed extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static String username;
    private static User user;
    private static String myUsername;
    private static String newsFeedUserName;
    UserData me;

    private OnFragmentInteractionListener mListener;
    View rootView;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    TabLayout tabLayout;
    int tabCount = 2;
    FloatingActionButton new_post_button;
    int currentPosition = -1;


    public NewsFeedTabbed() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewsFeedTabbed.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsFeedTabbed newInstance(String param1, String param2,User us) {
        NewsFeedTabbed fragment = new NewsFeedTabbed();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        myUsername = param2;
        newsFeedUserName = param1;
        fragment.setArguments(args);
        user = us;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_news_feed_tabbed, container, false);
        me = (UserData) getActivity().getApplicationContext();

        new GetFriendsUsername(new GetFriendsUsername.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                me.setFriends(output);
            }
        }).execute(me.username());

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        new_post_button = (FloatingActionButton) rootView.findViewById(R.id.fab1);
        new_post_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewPostFragment editNameDialog = new NewPostFragment();
                editNameDialog.show(getActivity().getSupportFragmentManager(), "fragment_edit_name");
            }
        });
        // Set up the ViewPager with the sections adapter.
        // (&) Check if you are your own feed or one another user.
        // (&) 2 for other 4 for your own feed.

        // Uncomment whem about me and tribes are finished...
        //if(!myUsername.equalsIgnoreCase(newsFeedUserName)){tabCount = 2;}

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        Log.d("Pager is set WOoot!","Pager");

        // Set up Tabs
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        setTabIcons();
        mSectionsPagerAdapter.getItem(0);

        // Inflate the layout for this fragment
        return rootView;
    }

    void setTabIcons(){
        if(myUsername.equalsIgnoreCase(newsFeedUserName))
        for(int i = 0; i < tabCount; i++)
        {
            switch (i)
            {
                case 0:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_feed);
                    break;
                case 1:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_shop);
                    break;
                case 2:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_profile);
                    break;
                case 3:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_profile);
                    break;
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Log.d("SetUpItem",String.valueOf(position));
            if(position == 0)
            {
                if(currentPosition == position)
                {
                    
                }
                currentPosition = position;
                return NewsFeed.newInstance(newsFeedUserName,myUsername,user,"");
            }
            else if(position == 1 )
            {
                if(user == null) {
                    currentPosition = position;
                    return ProductList.newInstance("user","",myUsername);
                }else{
                    currentPosition = position;
                    new_post_button.setVisibility(View.GONE);
                    return ProductList.newInstance("user","",user.username());
                }
            }
            else{
                currentPosition = position;
                return NewsFeed.newInstance(newsFeedUserName,myUsername,user,"");
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return tabCount;
        }

        Drawable myDrawable;
        String title;

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
            /*
            switch (position) {
                case 0:
                    myDrawable = getResources().getDrawable(R.drawable.ic_feed);
                    break;
                case 1:
                    myDrawable = getResources().getDrawable(R.drawable.ic_shop);
                    break;
                case 2:
                    myDrawable = getResources().getDrawable(R.drawable.ic_profile);
                    break;
                case 3:
                    myDrawable = getResources().getDrawable(R.drawable.ic_profile);
                    break;
                default:
                    break;
            }
            SpannableStringBuilder sb = new SpannableStringBuilder(""); // space added before text for convenience
            try {
                myDrawable.setBounds(4, 4, myDrawable.getIntrinsicWidth(), myDrawable.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(myDrawable, DynamicDrawableSpan.ALIGN_BASELINE);
                sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            } catch (Exception e) {
                // TODO: handle exception
            }
            return sb;
            */
        }
}
}
