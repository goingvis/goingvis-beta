package com.example.felix.goingvis_prototype;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import Helpers.Rest;
import model.UserData;

public class VerifyPhone extends AppCompatActivity {
    UserData user = null;
    EditText phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        user = (UserData) getApplicationContext();

        Button send = (Button) findViewById(R.id.send_verification);
        phoneNumber = (EditText) findViewById(R.id.phone_number_text);
    }

    void send_verification_code(View v) throws ExecutionException, InterruptedException {
        String number = phoneNumber.getText().toString();
        if(number.length() > 0){
            Rest r = new Rest();
            if (r.createMobileVerification( number,user.get_user_hash().get("username").toString() ) ){
                Intent i = new Intent(this,VerifyCode.class);
                startActivity(i);
            }
            else{
                Toast.makeText(this,"Please try again",Toast.LENGTH_SHORT);
            }
        }else{
            Toast.makeText(this,"Not a valid number",Toast.LENGTH_SHORT);
        }
    }


}
