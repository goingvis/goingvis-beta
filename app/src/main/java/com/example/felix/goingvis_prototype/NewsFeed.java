package com.example.felix.goingvis_prototype;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ExecutionException;

import Adapters.DeepReplyAdapter;
import Adapters.ReplyAdapter;
import Classes.DeepReply;
import Classes.Micropost;
import Classes.Reply;
import Classes.Utility;
import Helpers.DeepReplyDataStore;
import Helpers.ReplyDataStore;
import Helpers.Rest;
import model.DeepReplyData;
import model.MicropostData;
import model.ReplyData;
import model.User;
import model.UserData;
import Adapters.MicropostAdapter;
import RestActions.GetUserFeed;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsFeed.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewsFeed#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFeed extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private UserData user;
    private MicropostData micropostData;
    private List<Micropost> microposts = null;
    private ListView listView;
    private MicropostAdapter listAdapter;
    ReplyDataStore replyDataStore = new ReplyDataStore();
    DeepReplyDataStore deepReplyDataStore = new DeepReplyDataStore();
    String usernameOfNewsFeeder;
    static User nonUser = null;
    ViewPager viewPager;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FloatingActionButton PostButton;
    private View rootView;
    private int page_number = 1;
    Rest r = new Rest();

    private OnFragmentInteractionListener mListener;
    boolean dbListenerSet = false;

    // Firebase stuff
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    static public String specificMicropost = "";

    //DatabaseReference mConditionRef;

    public NewsFeed() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewsFeed.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsFeed newInstance(String param1, String param2,User user,String s) {
        NewsFeed fragment = new NewsFeed();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        nonUser = user;
        specificMicropost = s;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            usernameOfNewsFeeder = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_news_feed,container, false);
                // PostButton Logic

        // Inflate the layout for this fragment
        user = (UserData) getActivity().getApplicationContext();
        micropostData = new MicropostData();
        final View layout = inflater.inflate(R.layout.feed_item,container);
        listView = (ListView) rootView.findViewById(R.id.list);
        //viewPager = (ViewPager) rootView.findViewById(R.id.pager);

        final ProgressBar spinner = (ProgressBar) rootView.findViewById(R.id.load);
        spinner.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);



        new GetUserFeed(new GetUserFeed.AsyncResponse(){
            @Override
            public void processFinish(String output) throws JSONException, ExecutionException, InterruptedException {
                // Set specific micropost...
                if(specificMicropost.length() > 0){
                    nonUser = user.userClass();
                    usernameOfNewsFeeder = user.username();
                    micropostData.setSpecificMicropost(output);
                    rootView.setPadding(0,0,0,0);
                    rootView.setBackgroundColor(Color.parseColor("#ffffff"));
                    rootView.setMinimumWidth(Utility.screenHeight(getContext()));
                }else{
                    micropostData.setMicroposts(output);
                }
                // Setting microposts
                microposts = micropostData.get_micropost_list();

                listAdapter = new MicropostAdapter(getActivity(), microposts,layout,replyDataStore,deepReplyDataStore,usernameOfNewsFeeder,nonUser,specificMicropost);
                listAdapter.setApplicationContext(user);

                listView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();

                spinner.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);
                listView.setVisibility(View.VISIBLE);
                // Setting on click buttons

                // Connecting firebase
                database.child(user.id()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (!snapshot.exists())
                        {
                            // Create Snapshot
                            database.setValue(user.id());
                            // TODO: handle the case where the data already exists
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                // Done Now Returning the view.
                database.child(user.id()).setValue("null");
                // Setting Infinite Scroll
                listView = (ListView) rootView.findViewById(R.id.list);
                if(specificMicropost.length() == 0)
                {
                    listView.setOnScrollListener(new AbsListView.OnScrollListener() {

                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {
                            if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                                    && (listView.getLastVisiblePosition() - listView.getHeaderViewsCount() -
                                    listView.getFooterViewsCount()) >= (listAdapter.getCount() - 1)) {
                                page_number++;
                                GetUserFeed asyncTask = (GetUserFeed) new GetUserFeed(new GetUserFeed.AsyncResponse() {

                                    @Override
                                    public void processFinish(String output) throws JSONException {
                                        micropostData.setMicroposts(output);
                                        listAdapter.notifyDataSetChanged();
                                    }
                                }).execute(usernameOfNewsFeeder, page_number, specificMicropost);
                            }
                        }

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        }
                    });
                }
            }
        }).execute(usernameOfNewsFeeder,page_number,specificMicropost);

        ((MainActivity)getActivity()).create_notify_bubbles();

        return rootView;
    }

    public static NewsFeed newInstance(int sectionNumber) {
        NewsFeed fragment = new NewsFeed();
        Bundle args = new Bundle();
        args.putInt("NewsFeedSection", sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    // Making News Feed Listen To The Changes
    @Override
    public void onStart(){
        super.onStart();
        if(!dbListenerSet) {
            database.child(user.id()).addChildEventListener(new ChildEventListener() {

                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.exists()) {
                        String text = (String) dataSnapshot.getValue().toString();
                        if (!text.equalsIgnoreCase("null")) {
                            Log.d("Firebase Value: ", dataSnapshot.getValue().toString());
                            JSONObject data = new JSONObject((HashMap) dataSnapshot.getValue());
                            try {
                                Log.d("Inside of the news feed", "Actions");
                                news_feed_actions(data, text);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //Log.d("FIREBASE", text);
                        }
                        // Set back to null to clear queue
                        database.child(user.id()).setValue("null");
                    }
                    //questionText.setText(text);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            dbListenerSet = true;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void news_feed_actions(JSONObject data,String text) throws JSONException {
        Hashtable<String,String> hash = Utility.jsonToHash(data);
        Log.d("Hash table",hash.get("type").toString());

        String type = hash.get("type").toString();
        String jsonStr = text;
        String tag;
        String loveTag;
        String hateTag;
        TextView love;
        TextView hate;
        ReplyAdapter replyAdapter = null;
        DeepReplyAdapter deepReplyAdapter = null;
        ReplyData replyData = null;
        DeepReplyData deepReplyData = null;
        boolean setToNull = false;

        switch (type)
        {
            case "vote":
                loveTag =  "love_" + hash.get("class").toString() + "_" + hash.get("id");
                hateTag = "hate_" + hash.get("class").toString() + "_" + hash.get("id");

                if(hash.get("class").equalsIgnoreCase("reply"))
                {
                    Log.d("TAG",loveTag);
                    replyAdapter = replyDataStore.get_reply_adapter(hash.get("micropost_id"));
                    replyData = replyDataStore.get_reply_data(hash.get("micropost_id"));
                    View replyView = replyDataStore.get_reply_view(hash.get("micropost_id"));
                    Log.d("Grabbed","Adapters");
                    if(replyAdapter != null && replyData != null)
                    {
                        Log.d("About to Set","IN");
                        love = (TextView) replyView.findViewWithTag("love_reply_" + hash.get("id"));
                        hate = (TextView) replyView.findViewWithTag("hate_reply_" + hash.get("id"));
                        //TextView commentCount = (TextView) replyView.findViewWithTag(R.id.comment_count);
                        //commentCount.setText(hash.get("comments_count"));
                        love.setText(hash.get("love_count"));
                        hate.setText(hash.get("hate_count"));
                        //replyAdapter.notifyDataSetChanged();
                        Log.d("UPDATE","Complete");

                    }
                }
                else if(hash.get("class").equalsIgnoreCase("deep_reply"))
                {
                    deepReplyAdapter = deepReplyDataStore.get_reply_adapter(hash.get("reply_id"));
                    deepReplyData = deepReplyDataStore.get_reply_data(hash.get("reply_id"));
                    View replyView = deepReplyDataStore.get_reply_view(hash.get("reply_id"));
                    Log.d("Grabbed","Adapters");
                    if(deepReplyAdapter != null && deepReplyData != null)
                    {
                        Log.d("About to Set","IN");
                        love = (TextView) replyView.findViewWithTag("love_deep_reply_" + hash.get("id"));
                        hate = (TextView) replyView.findViewWithTag("hate_deep_reply_" + hash.get("id"));
                        //TextView commentCount = (TextView) replyView.findViewWithTag(R.id.comment_count);
                        //commentCount.setText(hash.get("comments_count"));
                        if(love != null && hate != null) {
                            love.setText(hash.get("love_count"));
                            hate.setText(hash.get("hate_count"));
                        }
                        List<DeepReply> listReplies = deepReplyData.get_reply_list();
                        for (int i = 0; i < listReplies.size(); i++)
                        {
                            if(listReplies.get(i).getId().equalsIgnoreCase(hash.get("id")) )
                            {
                                listReplies.get(i).love_count = hash.get("love_count");
                                listReplies.get(i).hate_count = hash.get("hate_count");
                                break;
                            }
                        }
                    }
                }
                else {
                    love = (TextView) rootView.findViewWithTag(loveTag);
                    hate = (TextView) rootView.findViewWithTag(hateTag);
                    if (love != null && hate != null) {
                        love.setText(hash.get("love_count"));
                        hate.setText(hash.get("hate_count"));
                        for (int i = 0; i < microposts.size(); i++)
                        {
                            if(microposts.get(i).getId().equalsIgnoreCase(hash.get("id")) )
                            {
                                Log.d("SET COUNT for HASH",Integer.toString(i));
                                microposts.get(i).love_count = hash.get("love_count");
                                microposts.get(i).hate_count = hash.get("hate_count");
                                break;
                            }
                        }
                    }
                }
                setToNull = true;
                break;
            case "vote_reply":
                Log.d("IN REPLY","VOTE");
                loveTag =  "love_" + hash.get("class").toString() + "_" + hash.get("id");
                hateTag = "hate_" + hash.get("class").toString() + "_" + hash.get("id");
                Log.d("TAG",loveTag);
                replyAdapter = replyDataStore.get_reply_adapter(hash.get("micropost_id"));
                replyData = replyDataStore.get_reply_data(hash.get("micropost_id"));
                Log.d("Grabbed","Adapters");
                if(replyAdapter != null && replyData != null)
                {
                    Log.d("About to Set","IN");
                    replyAdapter.love_count.setText(hash.get("love_count"));
                    replyAdapter.hate_count.setText(hash.get("hate_count"));
                    replyAdapter.notifyDataSetChanged();
                    Log.d("UPDATE","Complete");

                }
                setToNull = true;
                break;
            case "share":
                TextView share_cc = (TextView) rootView.findViewWithTag("share_micropost_" + hash.get("micropost_id"));
                share_cc.setText(hash.get("share_count"));
                setToNull = true;
                break;
            case "reply":
                replyAdapter = replyDataStore.get_reply_adapter(hash.get("micropost_id"));
                replyData = replyDataStore.get_reply_data(hash.get("micropost_id"));
                if(replyAdapter != null && replyData != null)
                {
                    String commentTag = "comment_micropost_" + hash.get("micropost_id");
                    TextView t = (TextView) rootView.findViewWithTag(commentTag);
                    if(t != null)
                    {
                        for (int i = 0; i < microposts.size(); i++)
                        {
                            if(microposts.get(i).getId().equalsIgnoreCase(hash.get("micropost_id")) )
                            {
                                Log.d("SET COUNT for HASH",Integer.toString(i));
                                microposts.get(i).comments_count = hash.get("micropost_comments_count");
                                break;
                            }
                        }
                        t.setText(hash.get("micropost_comments_count"));
                        //listAdapter.notifyDataSetChanged();
                    }
                    Log.d("About to Set","IN");
                    replyData.setRepliesHash(hash);
                    replyAdapter.notifyDataSetChanged();
                    Log.d("UPDATE","Complete");
                }
                setToNull = true;
                break;
            case "deep_reply":
                deepReplyAdapter = deepReplyDataStore.get_reply_adapter(hash.get("reply_id"));
                deepReplyData = deepReplyDataStore.get_reply_data(hash.get("reply_id"));
                replyAdapter = replyDataStore.get_reply_adapter(hash.get("micropost_id"));
                replyData = replyDataStore.get_reply_data(hash.get("micropost_id"));
                View replyView = replyDataStore.get_reply_view(hash.get("micropost_id"));

                if(deepReplyAdapter != null && deepReplyData != null)
                {
                    String commentTag = "comment_micropost_" + hash.get("micropost_id");
                    TextView t = (TextView) rootView.findViewWithTag(commentTag);
                    if(t != null)
                    {
                        for (int i = 0; i < microposts.size(); i++)
                        {
                            if( microposts.get(i).getId().equalsIgnoreCase(hash.get("micropost_id")) )
                            {
                                Log.d("SET COUNT for HASH",Integer.toString(i));
                                microposts.get(i).comments_count = hash.get("micropost_comments_count");
                                break;
                            }
                        }
                        t.setText(hash.get("micropost_comments_count"));
                        //listAdapter.notifyDataSetChanged();
                    }

                    if(replyAdapter != null && replyData != null)
                    {
                        List<Reply> replyList = replyData.get_reply_list();
                        for (int i = 0; i < replyList.size(); i++)
                        {
                            if(replyList.get(i).getId().equalsIgnoreCase(hash.get("reply_id")) )
                            {
                                Log.d("SET COUNT for HASH",Integer.toString(i));
                                replyList.get(i).comments_count = hash.get("reply_comments_count");
                                TextView comments = (TextView) replyView.findViewWithTag("comment_reply_" + hash.get("reply_id"));
                                comments.setText(hash.get("reply_comments_count"));
                                break;
                            }
                        }
                    }
                    Log.d("About to Set","IN");
                    deepReplyData.setRepliesHash(hash);
                    deepReplyAdapter.notifyDataSetChanged();
                }
                setToNull = true;
                break;
            case "new_micropost":
                micropostData.prependMicropostWithHash(hash);
                listAdapter.notifyDataSetChanged();
                setToNull  =true;
                break;
            case "deleteMicropost":
                if(microposts != null &&  micropostData.micropostList.size() >= Integer.parseInt(hash.get("position")))
                {
                    micropostData.micropostList.remove(Integer.parseInt(hash.get("position")));
                    listAdapter.notifyDataSetChanged();
                    setToNull = true;
                }
                break;
        }

        if(setToNull){/*database.child(user.id()).setValue("null");*/}

    }

    class MicropostComp implements Comparator<Micropost>
    {
        @Override
        public int compare(Micropost lhs, Micropost rhs) {
            return (lhs.getId().compareTo(rhs.getId()));
        }
    }

    /* Pager Section */

}
