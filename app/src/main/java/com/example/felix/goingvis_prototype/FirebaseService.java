package com.example.felix.goingvis_prototype;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import model.UserData;

/**
 * Created by felix on 10/17/16.
 */
public class FirebaseService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage message){
        Log.d("MessagedRecieved",message.getData().toString());
        Map<String,String> hash =  message.getData();

        String title = "Title";
        String description = "description";
        NotificationCompat.Builder builder;
        PendingIntent contentIntent;
        NotificationManager manager;
        Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        UserData user = (UserData) getApplicationContext();
        boolean notificationReady = false;

        switch (hash.get("type")) {
                                /* Specifically created notification... */
            case "notification":
                Intent notificationIntent = null;
                switch (hash.get("class")) {
                    case "message":
                        Log.d("type of", "message");
                        notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
                        break;
                    case "new_conversation":
                        // Puts extra so conversation is instantly loaded
                        notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
                        String[] arr = new String[]{"conversation", hash.get("id")};
                        notificationIntent.putExtra("fragment", arr);
                        title = hash.get("title");
                        description = hash.get("message");
                        notificationReady = true;
                        break;
                    case "new_message":
                        if ( (user.username() == null) || !user.getCurrentConversation().equalsIgnoreCase(hash.get("id"))) {
                            notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
                            String[] arr2 = new String[]{"conversation", hash.get("id")};
                            notificationIntent.putExtra("fragment", arr2);
                            title = hash.get("title");
                            description = hash.get("message");
                            notificationReady = true;
                        }
                        break;
                    case "at_mention":
                        notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
                        String[] arr3 = new String[]{"micropost", hash.get("id")};
                        notificationIntent.putExtra("fragment", arr3);
                        title = hash.get("title");
                        description = hash.get("message");
                        notificationReady = true;
                        break;
                }

                if(notificationReady) {
                    Log.d("notification", hash.toString());
                    builder =
                            new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.mipmap.logo_vis)
                                    .setContentTitle(title)
                                    .setContentText(description);

                    contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(contentIntent);
                    builder.setAutoCancel(true);

                    // Add as notification
                    manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    manager.notify(0, builder.build());
                    // Vibrate for 500 milliseconds
                    v.vibrate(500);
                }
                break;
        }

    }


}
