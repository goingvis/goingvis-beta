package com.example.felix.goingvis_prototype;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.Future;

import Adapters.ConversationAdapter;
import Classes.Utility;
import model.ChatData;
import model.UserData;
import RestActions.GetConversation;
import RestActions.NewMessage;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Conversation.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Conversation#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Conversation extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    View rootView;
    UserData user = null;
    ListView listView;
    String id;
    ConversationAdapter listAdapter;
    ChatData chatData = new ChatData();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    EditText messageInput;
    Button sendMessage;
    Boolean dbListnerSet = false;

    private OnFragmentInteractionListener mListener;

    public Conversation() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Conversation.
     */
    // TODO: Rename and change types and number of parameters
    public static Conversation newInstance(String param1, String param2) {
        Conversation fragment = new Conversation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            id = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_conversation, container, false);
        user = (UserData) getActivity().getApplicationContext();
        user.setCurrentConversation(mParam1);

        final ProgressBar load  = (ProgressBar) rootView.findViewById(R.id.loadConversation);
        load.setVisibility(View.VISIBLE);
        messageInput = (EditText) rootView.findViewById(R.id.newMessageInput);
        sendMessage = (Button) rootView.findViewById(R.id.sendNewMessage);

        new GetConversation(new GetConversation.AsyncResponse() {
            @Override
            public void processFinish(String output) throws JSONException {
                Log.d("Conversation Loaded",output);
                listView  = (ListView) rootView.findViewById(R.id.conversationList);
                chatData.setConversation(output);
                // Setting microposts

                listAdapter = new ConversationAdapter(getActivity(), chatData.chatList, user);
                listView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();
                load.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                // Set firebase stuff...
                database.child(user.id()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (!snapshot.exists())
                        {
                            database.setValue(user.id());
                            // TODO: handle the case where the data already exists
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                // Done Now Returning the view.
                database.child(user.id()).setValue("null");

            }
        }).execute(user.username(),mParam1);

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mess = messageInput.getText().toString();
                sendMessage.setClickable(false);
                messageInput.setClickable(false);
                messageInput.setText("");
                new NewMessage(new NewMessage.AsyncResponse() {
                    @Override
                    public void processFinish(String output) throws JSONException {
                        sendMessage.setClickable(true);
                        messageInput.setClickable(true);
                    }
                }).execute(mParam1,mess,user.username());
            }
        });

        ((MainActivity)getActivity()).create_notify_bubbles();

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("FragmentAct","detached");
        mListener = null;
    }

    @Override
    public void onStop(){
        Log.d("FragmentAct","On Stop");
        user.setCurrentConversation("null");
        super.onStop();
    }

    @Override
    public void onStart(){
        super.onStart();
        if(!dbListnerSet) {
            database.child(user.id()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Log.d("Child added", dataSnapshot.getValue().toString());
                    String text = dataSnapshot.getValue().toString();
                    if (!text.equalsIgnoreCase("null")) {
                        JSONObject data = new JSONObject((HashMap) dataSnapshot.getValue());
                        try {
                            Log.d("Conversation", "Actions");
                            message_functions(data, text);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Log.d("FIREBASE", text);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            dbListnerSet = true;
        }

    }

    void message_functions(JSONObject data,String text) throws JSONException
    {
        Hashtable<String,String> hash = Utility.jsonToHash(data);
        boolean setToNull = true;
        Log.d("Hash table",hash.get("type").toString());

        //int idFrom = Integer.valueOf(hash.get("id"));
        if(mParam1.contains("\n") || mParam1.contains("\r") )
        {
            mParam1 = mParam1.replace("\n", "").replace("\r", "");
        }

        if(hash.get("id") != null && hash.get("id").equalsIgnoreCase(mParam1)) {

            String type = hash.get("type").toString();
            switch (type) {
                case "new_message":
                    Log.d("MESSAGE","about to add");
                    if(chatData != null && listAdapter != null) {
                        chatData.setWithHash(hash);
                        listAdapter.notifyDataSetChanged();
                        if(user != null && user.currentConversation == mParam1) {
                            Future<JsonObject> js = Ion.with(getActivity().getApplicationContext())
                                    .load("https://www.goingvis.com/api/v1/specific_conversation_notification_remove")
                                    .setLogging("MyLogs", Log.DEBUG)
                                    .addQuery("conversation", mParam1)
                                    .addQuery("id", user.id())
                                    .addQuery("api_key", Utility.apiKey)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            if (result != null) {
                                                Log.d("DONE!", result.getAsString());
                                            }
                                        }
                                    });
                        }
                    }
                    //database.child(user.id()).setValue("null");
                    break;
            }
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
