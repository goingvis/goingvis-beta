package com.example.felix.goingvis_prototype;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import Helpers.TouchImageView;

public class FullScreenImageViewActivity extends AppCompatActivity {


//	ProgressDialog dialog;
	TouchImageView imag_news_phpto;
	ProgressBar progressBarLoading;
	String photo_url,photo_url_thum;
	Toolbar toolbar;


	public FullScreenImageViewActivity(){}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fullimageview);

		this.setTitle(getIntent().getStringExtra("title"));

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		toolbar.setTitle(getIntent().getStringExtra("title"));

		bindview();
	}

//	@Override
//	public boolean onOptionsItemSelected(MenuItem menuItem) {
//		switch (menuItem.getItemId()) {
//			case android.R.id.home:
////				Intent homeIntent = new Intent(this, HomeActivity.class);
////				homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////				startActivity(homeIntent);
//				onBackPressed();
//		}
//		return (super.onOptionsItemSelected(menuItem));
//	}
	
	private void bindview() {
		try {

			photo_url= getIntent().getStringExtra("photo_url");
			imag_news_phpto = (TouchImageView)findViewById(R.id.imag_news_phpto);
			progressBarLoading = (ProgressBar)findViewById(R.id.progressBarLoading);
			progressBarLoading.setVisibility(View.VISIBLE);
			Log.e("photo_url",photo_url.replaceAll(" ","%20"));
			Picasso.with(FullScreenImageViewActivity.this)
					.load(photo_url.replaceAll(" ","%20"))
					.placeholder(R.drawable.logo_new)
					.error(R.drawable.logo_new)
					.into(imag_news_phpto, new com.squareup.picasso.Callback() {
						@Override
						public void onSuccess() {
							progressBarLoading.setVisibility(View.GONE);
						}

						@Override
						public void onError() {
							progressBarLoading.setVisibility(View.GONE);
							imag_news_phpto.setImageResource(R.drawable.logo_new);
						}
					});


		}catch (Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case android.R.id.home:
				finish();
		}
		return super.onOptionsItemSelected(menuItem);
	}

	}
