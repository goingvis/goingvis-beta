package com.example.felix.goingvis_prototype;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import UtilityFunctions.ConnectionDetector;

public class WebviewActivity extends AppCompatActivity {

	private final static int FILECHOOSER_RESULTCODE = 1;
	private final static int FCR = 1;
	//Control Declaration
	ProgressDialog mProgressDialog;
//	ProgressDialog mUploadProgressDialog;
	WebView mWebView;
	//Util class Declaration
	ConnectionDetector mConnectionDetector;
	//Global variable Declaration
	String linkUrl;
	private ValueCallback<Uri> mUploadMessage;
	private String mCM;
	private ValueCallback<Uri> mUM;
	private ValueCallback<Uri[]> mUMA;
	Toolbar toolbar;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_webview_activity);
		Initialization();
		BindView();
	}

	private void Initialization() {

		mConnectionDetector= new ConnectionDetector(WebviewActivity.this);

		mProgressDialog = new ProgressDialog(WebviewActivity.this);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setMessage("Form is loading Please Wait...");

//		mUploadProgressDialog = new ProgressDialog(WebviewActivity.this);
	}

	private void BindView() {

		mWebView = (WebView) findViewById(R.id.linkWebView);

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle(getIntent().getStringExtra("title"));

		linkUrl = getIntent().getStringExtra("linkUrl");
		if (mConnectionDetector.isConnectingToInternet()){

			mWebView.getSettings().setJavaScriptEnabled(true);
			mWebView.getSettings().setBuiltInZoomControls(false);
			mWebView.getSettings().setDisplayZoomControls(false);
			mWebView.getSettings().setAllowFileAccess(true);
			if (18 < Build.VERSION.SDK_INT) {
				//18 = JellyBean MR2, KITKAT=19
				mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			}
			mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

			if(Build.VERSION.SDK_INT >= 21){
				mWebView.getSettings().setMixedContentMode(0);
				mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
			}else if(Build.VERSION.SDK_INT >= 19){
				mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
			}else if(Build.VERSION.SDK_INT >=11 && Build.VERSION.SDK_INT < 19){
				mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}

			mWebView.setWebViewClient(new Callback());

			mWebView.setWebChromeClient(new WebChromeClient() {

				@Override
				public void onProgressChanged(WebView view, int newProgress) {
					Log.e("onProgressChanged",newProgress+"");
					super.onProgressChanged(view, newProgress);

				}

				//For Android 3.0+
				public void openFileChooser(ValueCallback<Uri> uploadMsg){
					mUM = uploadMsg;
					Intent i = new Intent(Intent.ACTION_GET_CONTENT);
					i.addCategory(Intent.CATEGORY_OPENABLE);
					i.setType("image/*");
					WebviewActivity.this.startActivityForResult(Intent.createChooser(i,"File Chooser"), FCR);
				}

				// For Android 3.0+, above method not supported in some android 3+ versions, in such case we use this
				public void openFileChooser(ValueCallback uploadMsg, String acceptType){
					mUM = uploadMsg;
					Intent i = new Intent(Intent.ACTION_GET_CONTENT);
					i.addCategory(Intent.CATEGORY_OPENABLE);
					i.setType("*/*");
					WebviewActivity.this.startActivityForResult(
							Intent.createChooser(i, "File Browser"),
							FCR);
				}

				//For Android 4.1+
				public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture){
					mUM = uploadMsg;
					Intent i = new Intent(Intent.ACTION_GET_CONTENT);
					i.addCategory(Intent.CATEGORY_OPENABLE);
					i.setType("image/*");
					WebviewActivity.this.startActivityForResult(Intent.createChooser(i, "File Chooser"), WebviewActivity.FCR);
				}

				//For Android 5.0+
				public boolean onShowFileChooser(
                        WebView webView, ValueCallback<Uri[]> filePathCallback,
                        FileChooserParams fileChooserParams){
					if(mUMA != null){
						mUMA.onReceiveValue(null);
					}
					mUMA = filePathCallback;
					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					if(takePictureIntent.resolveActivity(WebviewActivity.this.getPackageManager()) != null){
						File photoFile = null;
						try{
							photoFile = createImageFile();
							takePictureIntent.putExtra("PhotoPath", mCM);
						}catch(IOException ex){
							Log.e("file choose 5.0+ err", "Image file creation failed", ex);
						}
						if(photoFile != null){
							mCM = "file:" + photoFile.getAbsolutePath();
							Log.e("file path 5.0+",mCM);
							takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
						}else{
							takePictureIntent = null;
						}
					}
					Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
					contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
					contentSelectionIntent.setType("image/*");
					Intent[] intentArray;
					if(takePictureIntent != null){
						intentArray = new Intent[]{takePictureIntent};
					}else{
						intentArray = new Intent[0];
					}

					Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
					chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
					chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
					chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
					startActivityForResult(chooserIntent, FCR);
					return true;
				}

			});


			mWebView.setWebViewClient(new myWebClient());
			mWebView.loadUrl(linkUrl);
		}else{
			mConnectionDetector.show_alert();
		}

	}

	// Create an image file
	private File createImageFile() throws IOException {
		@SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "img_"+timeStamp+"_";
		File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return File.createTempFile(imageFileName,".jpg",storageDir);
	}

	@Override
	public void onBackPressed() {
		if (mWebView.canGoBack()) {
			mWebView.goBack();
		} else {
			finish();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case android.R.id.home:
				finish();
		}
		return super.onOptionsItemSelected(menuItem);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
									Intent intent) {

		if(Build.VERSION.SDK_INT >= 21){
			Uri[] results = null;
			//Check if response is positive
			if(resultCode== Activity.RESULT_OK){
				if(requestCode == FCR){
					if(null == mUMA){
						return;
					}
					if(intent == null){
						//Capture Photo if no image available
						if(mCM != null){
							results = new Uri[]{Uri.parse(mCM)};
						}
					}else{
						String dataString = intent.getDataString();
						if(dataString != null){
							results = new Uri[]{Uri.parse(dataString)};
						}
					}
				}
			}
			mUMA.onReceiveValue(results);
			mUMA = null;
		}else{
			if(requestCode == FCR){
				if(null == mUM) return;
				Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
				mUM.onReceiveValue(result);
				mUM = null;
			}
		}



//		if(requestCode==FILECHOOSER_RESULTCODE)
//		{
//			if (null == mUploadMessage) return;
//			Uri result = intent == null || resultCode != RESULT_OK ? null
//					: intent.getData();
//			mUploadMessage.onReceiveValue(result);
//			mUploadMessage = null;
//		}
	}

	public class Callback extends WebViewClient {
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Toast.makeText(getApplicationContext(), "Failed loading app!", Toast.LENGTH_SHORT).show();
		}
	}

	public class myWebClient extends WebViewClient
	{
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			Log.e("onPageStarted",url);
				setProcessDialogueMessage("Loading Please Wait..");
			showProcessDialogue();
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.e("shouldOverrideUrlLoading", url);
			view.loadUrl(url);
			return true;

		}

		@Override
		public void onPageFinished(WebView view, String url) {
			Log.e("onPageFinished", "Finished loading URL: " + url);
			hideProcessDialogue();
			super.onPageFinished(view, url);
		}

		@Override
		public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
			Log.e("onReceivedError", "Error: " + error.toString());
			super.onReceivedError(view, request, error);
		}

	}

	public void showProcessDialogue(){
		if(!mProgressDialog.isShowing())
		mProgressDialog.show();
	}

	public void setProcessDialogueMessage(String message){
		mProgressDialog.setMessage(message);
	}

	public void hideProcessDialogue(){
		if(mProgressDialog.isShowing()){
			mProgressDialog.dismiss();
		}
	}

	private void showExitConfDialogue(String from){
		if(from.equals("success")){
			finish();
		}else{
			new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Exit")
					.setMessage("Are you sure you want to close?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}

					})
					.setNegativeButton("No", null)
					.show();
		}

	}

}